﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using MetaPostEditor.Model;

namespace MetaPostEditor.SaveModel
{
    public class ShapesList<TListType> : IXmlSerializable where TListType : IMpShape<Shape>
    {
        private readonly List<TListType> _list = new List<TListType>();

        public void Add(TListType item)
        {
            _list.Add(item);
        }

        public List<TListType> GetElements()
        {
            return _list.ToList();
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            
            while (true) 
            {
                reader.Read();
                var shape = MpShape.FromXmlElement(reader);
                if (shape == null)
                {
                    break;
                }

                shape.Deserialize(reader);
                _list.Add((TListType) shape);
            }
        }

        public void WriteXml(XmlWriter writer)
        {
            foreach (var shape in _list)
            {
                writer.WriteStartElement("Shape");
                shape.Serialize(writer);
                writer.WriteEndElement();
            }
        }
    }

    public class ProjectFileModel
    {
        public ShapesList<IMpShape<Shape>> Shapes { get; set; } = new ShapesList<IMpShape<Shape>>();
    }
}