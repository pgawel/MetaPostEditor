﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using MetaPostEditor.Model;

namespace MetaPostEditor.ViewModel.UserControls
{
    /// <summary>
    /// Interaction logic for PointDescription.xaml
    /// </summary>
    public partial class PointDescription
    {
        // Description Field Name
        public static readonly DependencyProperty DescriptionFieldNameProperty = 
            DependencyProperty.Register("DescriptionFieldName", typeof(string), typeof(PointDescription), 
                new FrameworkPropertyMetadata("Point description"));

        public string DescriptionFieldName
        {
            get
            {
                return GetValue(DescriptionFieldNameProperty).ToString();
            }
            set
            {
                SetValue(DescriptionFieldNameProperty, value);
            }
        }

        public static DependencyProperty MpPointInstanceProperty =
            DependencyProperty.Register("MpPointInstance", typeof(MpPoint), typeof(PointDescription),
                new FrameworkPropertyMetadata());

        public MpPoint MpPointInstance
        {
            get
            {
                return (MpPoint)GetValue(MpPointInstanceProperty);
            }
            set
            {
                SetValue(MpPointInstanceProperty, value);
            }
        }

        public ObservableCollection<DefaultShapeProperties.DescriptionAllignmentType> DescriptionAlignComboBox { get; set; }

        public PointDescription()
        {
            InitializeComponent();
            DataContext = this;

            // Set description alignment combobox items
            DescriptionAlignComboBox = new ObservableCollection<DefaultShapeProperties.DescriptionAllignmentType>();
            foreach (DefaultShapeProperties.DescriptionAllignmentType item in Enum.GetValues(typeof(DefaultShapeProperties.DescriptionAllignmentType)))
            {
                DescriptionAlignComboBox.Add(item);
            }
        }
    }
}
