﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MetaPostEditor.Model;

namespace MetaPostEditor.ViewModel.UserControls
{
    /// <summary>
    /// Interaction logic for MpLineUc.xaml
    /// </summary>
    public partial class MpLineUc
    {
        // MpLine instance dependency property
        public static DependencyProperty MpLineInstanceProperty =
            DependencyProperty.Register("MpLineInstance", typeof(MpLine), typeof(MpLineUc),
                new FrameworkPropertyMetadata());

        public MpLine MpLineInstance
        {
            get
            {
                return (MpLine)GetValue(MpLineInstanceProperty);
            }
            set
            {
                SetValue(MpLineInstanceProperty, value);
            }
        }

        public MpLineUc()
        {
            InitializeComponent();
            DataContext = this;
        }
    }
}
