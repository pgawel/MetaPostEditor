﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MetaPostEditor.Model;

namespace MetaPostEditor.ViewModel.UserControls
{
    /// <summary>
    /// Interaction logic for MpPolygonUc.xaml
    /// </summary>
    public partial class MpPolygonUc
    {
        // MpPolygon instance dependency property
        public static DependencyProperty MpPolygonInstanceProperty =
            DependencyProperty.Register("MpPolygonInstance", typeof(MpPolygon), typeof(MpPolygonUc),
                new FrameworkPropertyMetadata());

        public MpPolygon MpPolygonInstance
        {
            get
            {
                return (MpPolygon)GetValue(MpPolygonInstanceProperty);
            }
            set
            {
                SetValue(MpPolygonInstanceProperty, value);
            }
        }

        public MpPolygonUc()
        {
            InitializeComponent();
            DataContext = this;
        }
    }
}
