﻿using System.Windows;
using MetaPostEditor.Model;

namespace MetaPostEditor.ViewModel.UserControls
{
    /// <summary>
    /// Interaction logic for MpEllipseUc.xaml
    /// </summary>
    public partial class MpEllipseUc
    {
        // MpEllipse instance dependency property
        public static DependencyProperty MpEllipseInstanceProperty =
            DependencyProperty.Register("MpEllipseInstance", typeof(MpEllipse), typeof(MpEllipseUc),
                new FrameworkPropertyMetadata());

        public MpEllipse MpEllipseInstance
        {
            get
            {
                return (MpEllipse)GetValue(MpEllipseInstanceProperty);
            }
            set
            {
                SetValue(MpEllipseInstanceProperty, value);
            }
        }

        public MpEllipseUc()
        {
            InitializeComponent();
            DataContext = this;
        }
    }
}
