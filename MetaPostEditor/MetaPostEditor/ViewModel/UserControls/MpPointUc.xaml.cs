﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MetaPostEditor.Model;

namespace MetaPostEditor.ViewModel.UserControls
{
    /// <summary>
    /// Interaction logic for MpPointUc.xaml
    /// </summary>
    public partial class MpPointUc
    {
        // MpPoint instance dependency property
        public static DependencyProperty MpPointInstanceProperty =
            DependencyProperty.Register("MpPointInstance", typeof(MpPoint), typeof(MpPointUc),
                new FrameworkPropertyMetadata());

        public MpPoint MpPointInstance
        {
            get
            {
                return (MpPoint)GetValue(MpPointInstanceProperty);
            }
            set
            {
                SetValue(MpPointInstanceProperty, value);
            }
        }

        public MpPointUc()
        {
            InitializeComponent();
            DataContext = this;
        }
    }
}
