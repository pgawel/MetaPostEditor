﻿using System.Windows;
using MetaPostEditor.Model;

namespace MetaPostEditor.ViewModel.UserControls
{
    /// <summary>
    /// Interaction logic for MpRectangleUc.xaml
    /// </summary>
    public partial class MpRectangleUc
    {
        // MpRectangle instance dependency property
        public static DependencyProperty MpRectangleInstanceProperty =
            DependencyProperty.Register("MpRectangleInstance", typeof(MpRectangle), typeof(MpRectangleUc),
                new FrameworkPropertyMetadata());

        public MpRectangle MpRectangleInstance
        {
            get
            {
                return (MpRectangle)GetValue(MpRectangleInstanceProperty);
            }
            set
            { 
                SetValue(MpRectangleInstanceProperty, value);
            }
        }

        public MpRectangleUc()
        {
            InitializeComponent();
            DataContext = this;
        }
    }
}
