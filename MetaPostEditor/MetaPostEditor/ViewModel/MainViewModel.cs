﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Xml.Serialization;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MetaPostEditor.Model;
using MetaPostEditor.SaveModel;
using Microsoft.Win32;

namespace MetaPostEditor.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        public ObservableCollection<object> CanvasCollection { get; set; } = new ObservableCollection<object>();
        public ObservableCollection<IMpShape<Shape>> MpShapesCollection { get; set; } = new ObservableCollection<IMpShape<Shape>>();

        private IShape _selectedMpShape;
        public IShape SelectedMpShape
        {
            get { return _selectedMpShape; }
            set
            {
                Set(ref _selectedMpShape, value);
                UpdateSelectedInstance();
            }
        }

        #region Default properties form fields

        public int ShapeHeightTextBox
        {
            get { return DefaultShapeProperties.GetInstance.ShapeHeight; }
            set
            {
                if (value > 0)
                    DefaultShapeProperties.GetInstance.ShapeHeight = value;
            }
        }

        public int ShapeWidthTextBox
        {
            get { return DefaultShapeProperties.GetInstance.ShapeWidth; }
            set
            {
                if (value > 0)
                    DefaultShapeProperties.GetInstance.ShapeWidth = value;
            }
        }

        public Color FillColorPicker
        {
            get { return DefaultShapeProperties.GetInstance.FillColor; }
            set { DefaultShapeProperties.GetInstance.FillColor = value; }
        }

        public Color BorderColorPicker
        {
            get { return DefaultShapeProperties.GetInstance.BorderColor; }
            set { DefaultShapeProperties.GetInstance.BorderColor = value; }
        }

        public int BorderThicknessTextBox
        {
            get { return DefaultShapeProperties.GetInstance.BorderThickness; }
            set
            {
                if (value > 0)
                    DefaultShapeProperties.GetInstance.BorderThickness = value;
            }
        }
        #endregion

        #region Rectangle properties

        private MpRectangle _mpRectangle;
        public MpRectangle MpRectangleInstance
        {
            get { return _mpRectangle; }
            set { Set(ref _mpRectangle, value); }
        }

        private Visibility _rectanglePropertyVisibility;
        public Visibility RectanglePropertiesVisibility
        {
            get { return _rectanglePropertyVisibility; }
            set { Set(ref _rectanglePropertyVisibility, value); }
        }

        #endregion

        #region Polygon properties

        private MpPolygon _mpPolygon;
        public MpPolygon MpPolygonInstance
        {
            get { return _mpPolygon; }
            set { Set(ref _mpPolygon, value); }
        }

        private Visibility _polygonPropertyVisibility;
        public Visibility PolygonPropertiesVisibility
        {
            get { return _polygonPropertyVisibility; }
            set { Set(ref _polygonPropertyVisibility, value); }
        }

        #endregion

        #region Ellipse properties

        private MpEllipse _mpEllipse;
        public MpEllipse MpEllipseInstance
        {
            get { return _mpEllipse; }
            set { Set(ref _mpEllipse, value); }
        }

        private Visibility _ellipsePropertyVisibility;
        public Visibility EllipsePropertiesVisibility
        {
            get { return _ellipsePropertyVisibility; }
            set { Set(ref _ellipsePropertyVisibility, value); }
        }

        #endregion

        #region Line properties

        private MpLine _mpLine;
        public MpLine MpLineInstance
        {
            get { return _mpLine; }
            set { Set(ref _mpLine, value); }
        }

        private Visibility _linePropertyVisibility;
        public Visibility LinePropertiesVisibility
        {
            get { return _linePropertyVisibility; }
            set { Set(ref _linePropertyVisibility, value); }
        }

        #endregion

        #region Point properties

        private MpPoint _mpPoint;
        public MpPoint MpPointInstance
        {
            get { return _mpPoint; }
            set { Set(ref _mpPoint, value); }
        }

        private Visibility _pointPropertyVisibility;
        public Visibility PointPropertiesVisibility
        {
            get { return _pointPropertyVisibility; }
            set { Set(ref _pointPropertyVisibility, value); }
        }

        #endregion

        private int _shapeId;
        private bool _isProjectSaved = true;
        private bool _drag;
        private Point _movementStartPoint;
        private Point _lastLocation;

        public MainViewModel()
        {
            // Set property blocks visibility
            HideAllProperties();
        }

        /// <summary>
        /// Update SelectedMpShape value and Properties Block when user clicks on a shape on canvas.
        /// </summary>
        private void UpdateSelectedInstance()
        {
            HideAllProperties();

            if (_selectedMpShape is MpEllipse)
            {
                MpEllipseInstance = _selectedMpShape as MpEllipse;
                EllipsePropertiesVisibility = Visibility.Visible;
            }
            else if (_selectedMpShape is MpLine)
            {
                MpLineInstance = _selectedMpShape as MpLine;
                LinePropertiesVisibility = Visibility.Visible;
            }
            else if (_selectedMpShape is MpPoint)
            {
                MpPointInstance = _selectedMpShape as MpPoint;
                PointPropertiesVisibility = Visibility.Visible;
            }
            else if (_selectedMpShape is MpPolygon)
            {
                MpPolygonInstance = _selectedMpShape as MpPolygon;
                PolygonPropertiesVisibility = Visibility.Visible;
            }
            else if (_selectedMpShape is MpRectangle)
            {
                MpRectangleInstance = _selectedMpShape as MpRectangle;
                RectanglePropertiesVisibility = Visibility.Visible;
            }
        }

        /// <summary>
        /// Method triggered when mouse is over a shape on canvas.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MouseEnter(object sender, MouseEventArgs e)
        {
            var shape = (IShape)sender;
            shape.ShapeGeneral.Cursor = Cursors.Hand;
        }

        /// <summary>
        /// Method triggered when user clicks on a shape on canvas.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _drag = true;
            var shape = (IShape)sender;
            shape.ShapeGeneral.Cursor = Cursors.SizeAll;
            SelectedMpShape = shape;
            _movementStartPoint = e.GetPosition(VisualTreeHelper.GetParent(shape.ShapeGeneral) as IInputElement);
            _lastLocation = SelectedMpShape.GetPosition();
            Mouse.Capture(shape.ShapeGeneral);
        }

        /// <summary>
        /// Method responsible for moving selected object on canvas.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                if (!_drag) return;

                IShape shape = sender as IShape;

                if (shape != null)
                {
                    Shape element = shape.ShapeGeneral;
                    double shiftX = e.GetPosition(VisualTreeHelper.GetParent(element) as IInputElement).X - _movementStartPoint.X;
                    double shiftY = e.GetPosition(VisualTreeHelper.GetParent(element) as IInputElement).Y - _movementStartPoint.Y;
                    double newX = _movementStartPoint.X + shiftX;
                    double newY = _movementStartPoint.Y + shiftY;
                    Point offset = new Point(_movementStartPoint.X - _lastLocation.X, _movementStartPoint.Y - _lastLocation.Y);
                    double canvasX = newX - offset.X;
                    double canvasY = newY - offset.Y;
                    var canvas = (Canvas)VisualTreeHelper.GetParent(element);

                    SelectedMpShape.MoveTo(new Point(canvasX, canvasY), canvas.ActualHeight, canvas.ActualWidth);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw;
            }

            _isProjectSaved = false;
        }

        /// <summary>
        /// Method triggered when user releases mouse button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MouseUp(object sender, MouseButtonEventArgs e)
        {
            _drag = false;
            var shape = (IShape)sender;
            shape.ShapeGeneral.Cursor = Cursors.Arrow;
            Mouse.Capture(null);
        }

        /// <summary>
        /// Create new rectangle
        /// </summary>
        public ICommand NewRectangleCommand => new RelayCommand(() =>
        {
            MpRectangleInstance = new MpRectangle(_shapeId++, CanvasCollection);
            MpShapesCollection.Add(_mpRectangle);
            _mpRectangle.Draw();

            // Hide other property blocks
            HideAllProperties();

            // Set rectangle properties block visibility
            RectanglePropertiesVisibility = Visibility.Visible;

            // Add mouse handlers
            _mpRectangle.MouseEnter += MouseEnter;
            _mpRectangle.MouseLeftButtonDown += MouseLeftButtonDown;
            _mpRectangle.MouseMove += MouseMove;
            _mpRectangle.MouseUp += MouseUp;

            _isProjectSaved = false;
        });

        /// <summary>
        /// Create new polygon
        /// </summary>
        public ICommand NewPolygonCommand => new RelayCommand(() =>
        {
            MpPolygonInstance = new MpPolygon(_shapeId++, CanvasCollection);
            MpShapesCollection.Add(_mpPolygon);
            _mpPolygon.Draw();

            // Hide other property blocks
            HideAllProperties();

            // Set visibility
            PolygonPropertiesVisibility = Visibility.Visible;

            // Add mouse handlers
            _mpPolygon.MouseEnter += MouseEnter;
            _mpPolygon.MouseLeftButtonDown += MouseLeftButtonDown;
            _mpPolygon.MouseMove += MouseMove;
            _mpPolygon.MouseUp += MouseUp;

            _isProjectSaved = false;
        });

        /// <summary>
        /// Create new ellipse
        /// </summary>
        public ICommand NewEllipseCommand => new RelayCommand(() =>
        {
            MpEllipseInstance = new MpEllipse(_shapeId++, CanvasCollection);
            MpShapesCollection.Add(_mpEllipse);
            _mpEllipse.Draw();

            // Hide other property blocks
            HideAllProperties();

            // Set visibility
            EllipsePropertiesVisibility = Visibility.Visible;

            // Add mouse handlers
            _mpEllipse.MouseEnter += MouseEnter;
            _mpEllipse.MouseLeftButtonDown += MouseLeftButtonDown;
            _mpEllipse.MouseMove += MouseMove;
            _mpEllipse.MouseUp += MouseUp;

            _isProjectSaved = false;
        });

        /// <summary>
        /// Create new line
        /// </summary>
        public ICommand NewLineCommand => new RelayCommand(() =>
        {
            MpLineInstance = new MpLine(_shapeId++, CanvasCollection);
            MpShapesCollection.Add(_mpLine);
            _mpLine.Draw();

            // Hide other property blocks
            HideAllProperties();

            // Set visibility
            LinePropertiesVisibility = Visibility.Visible;

            // Add mouse handlers
            _mpLine.MouseEnter += MouseEnter;
            _mpLine.MouseLeftButtonDown += MouseLeftButtonDown;
            _mpLine.MouseMove += MouseMove;
            _mpLine.MouseUp += MouseUp;

            _isProjectSaved = false;
        });

        /// <summary>
        /// Create new point
        /// </summary>
        public ICommand NewPointCommand => new RelayCommand(() =>
        {
            MpPointInstance = new MpPoint(_shapeId++, new Point(DefaultShapeProperties.GetInstance.StartPoint.X, DefaultShapeProperties.GetInstance.StartPoint.Y), CanvasCollection);
            MpShapesCollection.Add(_mpPoint);
            _mpPoint.Draw();

            // Hide other property blocks
            HideAllProperties();

            // Set visibility
            PointPropertiesVisibility = Visibility.Visible;

            // Add mouse handlers
            _mpPoint.MouseEnter += MouseEnter;
            _mpPoint.MouseLeftButtonDown += MouseLeftButtonDown;
            _mpPoint.MouseMove += MouseMove;
            _mpPoint.MouseUp += MouseUp;

            _isProjectSaved = false;
        });

        /// <summary>
        /// Remove selected item from canvas
        /// </summary>
        public ICommand RemoveSelectedItemCommand => new RelayCommand(() =>
        {
            if (SelectedMpShape == null)
            {
                MessageBox.Show("You have to select a shape in order to remove it. ", "Error: No shape selected", MessageBoxButton.OK);
            }

            HideAllProperties();

            if (_selectedMpShape is MpEllipse)
            {
                CanvasCollection.Remove(MpEllipseInstance.ShapeInstance);
                MpEllipseInstance.StartPoint.Remove();
                MpShapesCollection.Remove(MpEllipseInstance);
            }
            else if (_selectedMpShape is MpLine)
            {
                CanvasCollection.Remove(MpLineInstance.ShapeInstance);
                MpLineInstance.StartPoint.Remove();
                MpLineInstance.EndPoint.Remove();
                MpShapesCollection.Remove(MpLineInstance);
            }
            else if (_selectedMpShape is MpPoint)
            {
                CanvasCollection.Remove(MpPointInstance.PointMarker);
                CanvasCollection.Remove(MpPointInstance.DescriptionTextBlock);
                MpShapesCollection.Remove(MpPointInstance);
            }
            else if (_selectedMpShape is MpPolygon)
            {
                CanvasCollection.Remove(MpPolygonInstance.ShapeInstance);
                MpPolygonInstance.VertexA.Remove();
                MpPolygonInstance.VertexB.Remove();
                MpPolygonInstance.VertexC.Remove();
                MpShapesCollection.Remove(MpPolygonInstance);
            }
            else if (_selectedMpShape is MpRectangle)
            {
                CanvasCollection.Remove(MpRectangleInstance.ShapeInstance);
                MpRectangleInstance.StartPoint.Remove();
                MpRectangleInstance.VertexB.Remove();
                MpRectangleInstance.VertexC.Remove();
                MpRectangleInstance.VertexD.Remove();
                MpShapesCollection.Remove(MpRectangleInstance);
            }
        });

        /// <summary>
        /// Generate MetaPost file
        /// </summary>
        public ICommand GenerateMetaPostFileCommand => new RelayCommand(() =>
        {
            // Open file dialog
            var fileDialog = new SaveFileDialog
            {
                Filter = "Mp(*Mp)|*.mp",
                AddExtension = true
            };

            var showDialog = fileDialog.ShowDialog();
            if (showDialog == null || !showDialog.Value) return;

            // Create file
            string path = fileDialog.FileName;
            string[] lines =
            {
                "prologues:=3;",
                "verbatimtex",
                "%&latex",
                "\\documentclass{minimal}",
                "\\begin{document}",
                "etex",
                "beginfig(1);",
                "endfig;",
                "end;"
            };
            File.WriteAllLines(path, lines);

            foreach (var mpShape in MpShapesCollection)
            {
                mpShape.ConvertIntoMetaPost(path);
            }
        });

        /// <summary>
        /// New project command - remove all elements and clear canvas
        /// </summary>
        public ICommand NewProjectCommand => new RelayCommand(() =>
        {
            if (!_isProjectSaved)
            {
                var result = MessageBox.Show("Would you like to save your project?", "Project is not saved.", MessageBoxButton.YesNoCancel);
                if (result == MessageBoxResult.Yes)
                {
                    SaveProject();
                    _isProjectSaved = true;
                }
                else if (result == MessageBoxResult.No)
                {
                    _isProjectSaved = true;
                }
                else if (result == MessageBoxResult.Cancel)
                {
                    return;
                }
                _isProjectSaved = true;
            }

            CanvasCollection.Clear();
            MpShapesCollection.Clear();
            _shapeId = 0;
        });

        /// <summary>
        /// Generate project xml file (serialization)
        /// </summary>
        public ICommand SaveProjectCommand => new RelayCommand(SaveProject);
        
        /// <summary>
        /// Close application 
        /// </summary>
        public ICommand CloseWindowCommand => new RelayCommand<Window>(CloseWindow);

        /// <summary>
        /// Read project xml file (deserialization)
        /// </summary>
        public ICommand OpenProjectCommand => new RelayCommand(() =>
        {
            if (!_isProjectSaved)
            {
                var result = MessageBox.Show("Would you like to save your project?", "Project is not saved.", MessageBoxButton.YesNoCancel);
                if (result == MessageBoxResult.Yes)
                {
                    SaveProject();
                    _isProjectSaved = true;
                }
                else if (result == MessageBoxResult.No)
                {
                    _isProjectSaved = true;
                }
                else if (result == MessageBoxResult.Cancel)
                {
                    return;
                }
                _isProjectSaved = true;
            }

            var fileDialog = new OpenFileDialog();
            fileDialog.Filter = "Image files (*.xml) | *.xml";
            if (fileDialog.ShowDialog() == true)
            {
                using (FileStream fs = File.OpenRead(fileDialog.FileName))
                {
                    CanvasCollection.Clear();
                    MpShapesCollection.Clear();

                    var xmlDeserializer = new XmlSerializer(typeof(ProjectFileModel));
                    var project = (ProjectFileModel)xmlDeserializer.Deserialize(fs);
                    project.Shapes.GetElements().ForEach(e =>
                    {
                        MpShapesCollection.Add(e);
                        e.InitializeForDrawing(CanvasCollection);
                        e.Draw();

                        e.MouseEnter += MouseEnter;
                        e.MouseLeftButtonDown += MouseLeftButtonDown;
                        e.MouseMove += MouseMove;
                        e.MouseUp += MouseUp;
                    });
                }
            }
        });

        private void SaveProject()
        {
            // Open file dialog
            var fileDialog = new SaveFileDialog
            {
                Filter = "Xml(*Xml)|*.xml",
                AddExtension = true
            };

            var showDialog = fileDialog.ShowDialog();
            if (showDialog == null || !showDialog.Value) return;

            // Create file
            using (Stream stream = File.Create(fileDialog.FileName))
            {
                var project = new ProjectFileModel();
                foreach (var mpShape in MpShapesCollection)
                {
                    project.Shapes.Add(mpShape);
                }

                var xmlSerializer = new XmlSerializer(typeof(ProjectFileModel));
                xmlSerializer.Serialize(stream, project);

                _isProjectSaved = true;
            }
        }

        private void CloseWindow(Window window)
        {
            if (_isProjectSaved)
            {
                CanvasCollection.Clear();
                MpShapesCollection.Clear();
                _shapeId = 0;
                _isProjectSaved = false;
            }
            else
            {
                var result = MessageBox.Show("Would you like to save your project?", "Project is not saved.", MessageBoxButton.YesNoCancel);
                if (result == MessageBoxResult.Yes)
                {
                    SaveProject();
                    _isProjectSaved = true;
                }
                else if (result == MessageBoxResult.No)
                {
                    _isProjectSaved = true;
                }
                else if (result == MessageBoxResult.Cancel)
                {
                    return;
                }
                _isProjectSaved = true;
                window?.Close();
            }
        }

        /// <summary>
        /// Hides other property blocks
        /// </summary>
        private void HideAllProperties()
        {
            RectanglePropertiesVisibility = Visibility.Collapsed;
            PolygonPropertiesVisibility = Visibility.Collapsed;
            EllipsePropertiesVisibility = Visibility.Collapsed;
            LinePropertiesVisibility = Visibility.Collapsed;
            PointPropertiesVisibility = Visibility.Collapsed;
        }

        #region Data validation
        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "ShapeHeightTextBox":
                        if (ShapeHeightTextBox < 0)
                            return "The height value cannot be negative.";
                        break;
                    case "ShapeWidthTextBox":
                        if (ShapeWidthTextBox < 0)
                            return "The width value cannot be negative.";
                        break;
                    case "BorderThicknessTextBox":
                        if (BorderThicknessTextBox < 0)
                            return "The border thickness value cannot be negative.";
                        break;
                }

                return string.Empty;
            }
        }

        public string Error { get; }
        #endregion
    }
}