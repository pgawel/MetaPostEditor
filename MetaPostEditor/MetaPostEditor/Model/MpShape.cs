﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Serialization;
using GalaSoft.MvvmLight;
using MouseEventArgs = System.Windows.Input.MouseEventArgs;
using MouseEventHandler = System.Windows.Input.MouseEventHandler;

namespace MetaPostEditor.Model
{
    [DataContract]
    public abstract class MpShape<TBaseShape> : ObservableObject, IMpShape<TBaseShape> where TBaseShape : Shape, new()
    {
        public int Id { get; set; }
        public TBaseShape ShapeInstance { get; private set; }
        public Shape ShapeGeneral => ShapeInstance;

        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                Draw();
            }
        }

        private string Type => GetType().ToString();

        public event MouseEventHandler MouseEnter;
        public event MouseButtonEventHandler MouseLeftButtonDown;
        public event MouseEventHandler MouseMove;
        public event MouseButtonEventHandler MouseUp;

        protected MpShape()
        {
            ShapeInstance = new TBaseShape();
            SubscribeToEvents();
        }

        protected MpShape(TBaseShape instance)
        {
            ShapeInstance = instance;
            SubscribeToEvents();
        }

        private void SubscribeToEvents()
        {
            ShapeInstance.MouseEnter += ShapeInstance_MouseEnter;
            ShapeInstance.MouseLeftButtonDown += ShapeInstance_MouseLeftButtonDown;
            ShapeInstance.MouseMove += ShapeInstance_MouseMove;
            ShapeInstance.MouseUp += ShapeInstance_MouseUp;
        }

        private void ShapeInstance_MouseEnter(object sender, MouseEventArgs e)
        {
            MouseEnter?.Invoke(this, e);
        }

        private void ShapeInstance_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            MouseLeftButtonDown?.Invoke(this, e);
        }

        private void ShapeInstance_MouseUp(object sender, MouseButtonEventArgs e)
        {
            MouseUp?.Invoke(this, e);
        }

        private void ShapeInstance_MouseMove(object sender, MouseEventArgs e)
        {
            MouseMove?.Invoke(this, e);
        }

        protected MpShape(int id, string name, TBaseShape instance) : this(instance)
        {
            Id = id;
            _name = name;
        }

        public abstract void MoveTo(Point point, double canvasHeight, double canvasWidth);

        public abstract void InitializeForDrawing(ObservableCollection<object> canvasCollection);

        public abstract void Draw();

        public abstract Point GetPosition();

        public abstract void ConvertIntoMetaPost(string path);

        /// <summary>
        /// Method that returns corresponding MetaPost alignment names to custom alignment names.
        /// </summary>
        /// <param name="descriptionAllignmentType"></param>
        /// <returns></returns>
        protected string SpecifyPointDescPositionInMetaPost(DefaultShapeProperties.DescriptionAllignmentType descriptionAllignmentType)
        {
            switch (descriptionAllignmentType)
            {
                case DefaultShapeProperties.DescriptionAllignmentType.Top:
                    return "top";
                case DefaultShapeProperties.DescriptionAllignmentType.Right:
                    return "rt";
                case DefaultShapeProperties.DescriptionAllignmentType.Bottom:
                    return "bot";
                case DefaultShapeProperties.DescriptionAllignmentType.Left:
                    return "lft";
                case DefaultShapeProperties.DescriptionAllignmentType.TopRight:
                    return "urt";
                case DefaultShapeProperties.DescriptionAllignmentType.BottomRight:
                    return "lrt";
                case DefaultShapeProperties.DescriptionAllignmentType.BottomLeft:
                    return "llft";
                case DefaultShapeProperties.DescriptionAllignmentType.TopLeft:
                    return "ulft";
                default:
                    return "err";
            }
        }

        public virtual void Serialize(XmlWriter writer)
        {
            writer.WriteAttributeString("id", Id.ToString());
            writer.WriteAttributeString("name", Name);
            writer.WriteAttributeString("type", GetType().Name);
        }

        public virtual void Deserialize(XmlReader reader)
        {
            reader.MoveToAttribute("id");
            Id = reader.ReadContentAsInt();
            reader.MoveToAttribute("name");
            _name = reader.ReadContentAsString();
            reader.Read();
        }
    }

    public static class MpShape
    {
        public static IMpShape<Shape> FromXmlElement(XmlReader reader)
        {
            reader.MoveToAttribute("type");
            string typeStr = typeof(MpShape).Namespace + '.' + reader.Value;
            Type t = Type.GetType(typeStr);
            if (t != null)
            {
                IMpShape<Shape> shape = (IMpShape<Shape>)t.Assembly.CreateInstance(typeStr);
                return shape;
            }
            return null;
        }
    }
}
