﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Serialization;

namespace MetaPostEditor.Model
{
    [DataContract]
    public class MpRectangle : MpShape<Rectangle>, IDataErrorInfo
    {
        #region Properties

        #region StartPoint properties
        private MpPoint _startPoint;
        public MpPoint StartPoint
        {
            get { return _startPoint; }
            set
            {
                RemoveVertices();
                Set(ref _startPoint, value);
                SetVertices(value.Position);
                Draw();
                RaisePropertyChanged(string.Empty);
            }
        }

        private double _startPointX;
        public double StartPointX
        {
            get { return _startPointX; }
            set
            {
                if (value < 0)
                    return;
                var oldStartPoint = StartPoint;
                RemoveVertices();
                Set(ref _startPointX, value);
                StartPoint = new MpPoint(new Point(value, _startPointY), _canvasCollection)
                {
                    Description = oldStartPoint.Description,
                    DescriptionAlignment = oldStartPoint.DescriptionAlignment,
                    DescriptionShift = oldStartPoint.DescriptionShift,
                    FillColor = oldStartPoint.FillColor
                };
            }
        }

        private double _startPointY;
        public double StartPointY
        {
            get { return _startPointY; }
            set
            {
                if (value < 0)
                    return;
                var oldStartPoint = StartPoint;
                RemoveVertices();
                Set(ref _startPointY, value);
                StartPoint = new MpPoint(new Point(_startPointX, value), _canvasCollection)
                {
                    Description = oldStartPoint.Description,
                    DescriptionAlignment = oldStartPoint.DescriptionAlignment,
                    FillColor = oldStartPoint.FillColor
                };
            }
        }
        #endregion

        private MpPoint _vertexB;
        public MpPoint VertexB
        {
            get { return _vertexB; }
            set
            {
                Set(ref _vertexB, value);
                Draw();
            }
        }

        private MpPoint _vertexC;
        public MpPoint VertexC
        {
            get { return _vertexC; }
            set
            {
                Set(ref _vertexC, value);
                Draw();
            }
        }

        private MpPoint _vertexD;
        public MpPoint VertexD
        {
            get { return _vertexD; }
            set
            {
                Set(ref _vertexD, value);
                Draw();
            }
        }

        private int _shapeHeight;
        public int ShapeHeight
        {
            get { return _shapeHeight; }
            set
            {
                if (value < 0)
                    return;
                Set(ref _shapeHeight, value);
                SetVertices(_startPoint.Position);
                Draw();
            }
        }

        private int _shapeWidth;
        public int ShapeWidth
        {
            get { return _shapeWidth; }
            set
            {
                if (value < 0)
                    return;
                Set(ref _shapeWidth, value);
                SetVertices(_startPoint.Position);
                Draw();
            }
        }

        private Color _fillColor;
        public Color FillColor
        {
            get { return _fillColor; }
            set
            {
                _fillColor = value;
                Draw();
            }
        }

        private Color _borderColor;
        public Color BorderColor
        {
            get { return _borderColor; }
            set
            {
                _borderColor = value;
                Draw();
            }
        }

        private int _borderThickness;
        public int BorderThickness
        {
            get { return _borderThickness; }
            set
            {
                if (value < 0)
                    return;
                _borderThickness = value;
                Draw();
            }
        }

        #endregion

        private ObservableCollection<object> _canvasCollection;
        private bool _isAlreadyOnCanvas;

        /// <summary>
        /// Empty constructor
        /// </summary>
        public MpRectangle() { }

        /// <summary>
        /// Constructor that initializes object properties
        /// </summary>
        /// <param name="id">Shape identification number</param>
        /// <param name="canvasCollection"></param>
        public MpRectangle(int id, ObservableCollection<object> canvasCollection)
            : base(id, "Rectangle" + id, new Rectangle())
        {
            _canvasCollection = canvasCollection;
            _startPoint = new MpPoint(DefaultShapeProperties.GetInstance.StartPoint, _canvasCollection);
            _startPointX = DefaultShapeProperties.GetInstance.StartPoint.X;
            _startPointY = DefaultShapeProperties.GetInstance.StartPoint.Y;
            _shapeHeight = DefaultShapeProperties.GetInstance.ShapeHeight;
            _shapeWidth = DefaultShapeProperties.GetInstance.ShapeWidth;
            _fillColor = DefaultShapeProperties.GetInstance.FillColor;
            _borderColor = DefaultShapeProperties.GetInstance.BorderColor;
            _borderThickness = DefaultShapeProperties.GetInstance.BorderThickness;

            _vertexB = new MpPoint(new Point(_startPointX + ShapeWidth, _startPointY), _canvasCollection)
            {
                Description = "B"
            };
            _vertexC = new MpPoint(new Point(_startPointX + ShapeWidth, _startPointY + ShapeHeight), _canvasCollection)
            {
                Description = "C",
                DescriptionAlignment = DefaultShapeProperties.DescriptionAllignmentType.Bottom
            };

            _vertexD = new MpPoint(new Point(_startPointX, _startPointY + ShapeHeight), _canvasCollection)
            {
                Description = "D",
                DescriptionAlignment = DefaultShapeProperties.DescriptionAllignmentType.Bottom
            };

            SetVertices(new Point(_startPointX, _startPointY));
            _canvasCollection.Add(ShapeInstance);
            _isAlreadyOnCanvas = true;

        }

        /// <summary>
        /// Method initializing current object for drawing for deserialization purposes
        /// </summary>
        /// <param name="canvasCollection"></param>
        public override void InitializeForDrawing(ObservableCollection<object> canvasCollection)
        {
            _canvasCollection = canvasCollection;
            StartPoint.InitializeForDrawing(canvasCollection);
            VertexB.InitializeForDrawing(canvasCollection);
            VertexC.InitializeForDrawing(canvasCollection);
            VertexD.InitializeForDrawing(canvasCollection); 
        }

        /// <summary>
        /// Draws current object
        /// </summary>
        public override void Draw()
        {
            SetProperties();
            _startPoint.Draw();
            _vertexB.Draw();
            _vertexC.Draw();
            _vertexD.Draw();

            if (_isAlreadyOnCanvas) return;
            _canvasCollection.Add(ShapeInstance);
            _isAlreadyOnCanvas = true; 
        }

        /// <summary>
        /// Sets current shape properties
        /// </summary>
        private void SetProperties()
        {
            ShapeInstance.Margin = new Thickness(_startPoint.PositionX, _startPoint.PositionY, 0, 0);
            ShapeInstance.Height = ShapeHeight;
            ShapeInstance.Width = ShapeWidth;
            ShapeInstance.Fill = new SolidColorBrush(FillColor);
            ShapeInstance.Stroke = new SolidColorBrush(BorderColor);
            ShapeInstance.StrokeThickness = BorderThickness;
        }

        /// <summary>
        /// Sets and marks vertices of the rectangle
        /// </summary>
        /// <param name="point"></param>
        public void SetVertices(Point point)
        {
            var oldStartPoint = _startPoint;
            var oldVertexB = _vertexB;
            var oldVertexC = _vertexC;
            var oldVertexD = _vertexD;

            RemoveVertices();

            _startPoint = new MpPoint(_canvasCollection)
            {
                Position = new Point(point.X, point.Y),
                Description = oldStartPoint.Description,
                DescriptionAlignment = oldStartPoint.DescriptionAlignment,
                DescriptionShift = oldStartPoint.DescriptionShift,
                ShapeHeight = oldStartPoint.ShapeHeight,
                FillColor = oldStartPoint.FillColor
            };

            _startPointX = point.X;
            _startPointY = point.Y;

            _vertexB = new MpPoint(new Point(point.X + _shapeWidth, point.Y), _canvasCollection)
            {
                Description = oldVertexB.Description,
                DescriptionAlignment = oldVertexB.DescriptionAlignment,
                FillColor = oldVertexB.FillColor
            };

            _vertexC = new MpPoint(new Point(point.X + _shapeWidth, point.Y + _shapeHeight), _canvasCollection)
            {
                Description = oldVertexC.Description,
                DescriptionAlignment = oldVertexC.DescriptionAlignment,
                FillColor = oldVertexC.FillColor
            };

            _vertexD = new MpPoint(new Point(point.X, point.Y + _shapeHeight), _canvasCollection)
            {
                Description = oldVertexD.Description,
                DescriptionAlignment = oldVertexD.DescriptionAlignment,
                FillColor = oldVertexD.FillColor
            };
        }

        /// <summary>
        /// Remove points from canvas. Main usage: remove previous point while moving the object
        /// </summary>
        public void RemoveVertices()
        {
            _startPoint.Remove();
            _vertexB.Remove();
            _vertexC.Remove();
            _vertexD.Remove();
        }

        /// <summary>
        /// Return current shape position (upper left vertex)
        /// </summary>
        /// <returns>Current shape position</returns>
        public override Point GetPosition()
        {
            return StartPoint.Position;
        }

        /// <summary>
        /// Move current object to given location
        /// </summary>
        /// <param name="point"></param>
        /// <param name="canvasHeight"></param>
        /// <param name="canvasWidth"></param>
        public override void MoveTo(Point point, double canvasHeight, double canvasWidth)
        {
            var shift = _startPoint.DescriptionShift;

            if (point.X + _shapeWidth + shift > canvasWidth || point.Y + _shapeHeight + shift > canvasHeight || point.X - shift < 0 || point.Y - shift < 0)
            {
                return;
            }

            StartPointX = point.X;
            StartPointY = point.Y;
        }

        /// <summary>
        /// Converts current object into MetaPost language and saves it in a file
        /// </summary>
        public override void ConvertIntoMetaPost(string path)
        {
            NumberFormatInfo nfi = new NumberFormatInfo { NumberDecimalSeparator = "." };
            double yMax = 842;

            // Verticles
            string strPositionX = Convert.ToString(_startPoint.Position.X);
            string strPositionY = Convert.ToString(yMax - _startPoint.Position.Y);
            string strPositionXplusWidth = Convert.ToString(_startPoint.Position.X + _shapeWidth);
            string strPositionYplusHeight = Convert.ToString(yMax - _startPoint.Position.Y - _shapeHeight);

            // Border color in RGB
            double borderColorR = Math.Round(_borderColor.R / 255.0, 3);
            double borderColorG = Math.Round(_borderColor.G / 255.0, 3);
            double borderColorB = Math.Round(_borderColor.B / 255.0, 3);

            // Fill color in RGB
            double fillColorR = Math.Round(_fillColor.R / 255.0, 3);
            double fillColorG = Math.Round(_fillColor.G / 255.0, 3);
            double fillColorB = Math.Round(_fillColor.B / 255.0, 3);

            // Start point fill color in RGB
            double startPointFillColorR = Math.Round(_startPoint.FillColor.R / 255.0, 3);
            double startPointFillColorG = Math.Round(_startPoint.FillColor.G / 255.0, 3);
            double startPointFillColorB = Math.Round(_startPoint.FillColor.B / 255.0, 3);

            // Vertex B fill color in RGB
            double vertexBFillColorR = Math.Round(_vertexB.FillColor.R / 255.0, 3);
            double vertexBFillColorG = Math.Round(_vertexB.FillColor.G / 255.0, 3);
            double vertexBFillColorB = Math.Round(_vertexB.FillColor.B / 255.0, 3);

            // Vertex C fill color in RGB
            double vertexCFillColorR = Math.Round(_vertexC.FillColor.R / 255.0, 3);
            double vertexCFillColorG = Math.Round(_vertexC.FillColor.G / 255.0, 3);
            double vertexCFillColorB = Math.Round(_vertexC.FillColor.B / 255.0, 3);

            // Vertex D fill color in RGB
            double vertexDFillColorR = Math.Round(_vertexD.FillColor.R / 255.0, 3);
            double vertexDFillColorG = Math.Round(_vertexD.FillColor.G / 255.0, 3);
            double vertexDFillColorB = Math.Round(_vertexD.FillColor.B / 255.0, 3);

            string[] mpCommands =
            {
                $"% --- Rectangle: Shape id: {Id} | Name: {Name} ---",
                // Draw rectangle
                $"draw ({strPositionX},{strPositionY})--({strPositionXplusWidth},{strPositionY})--({strPositionXplusWidth},{strPositionYplusHeight})--({strPositionX},{strPositionYplusHeight})--cycle withcolor({borderColorR.ToString(nfi)},{borderColorG.ToString(nfi)},{borderColorB.ToString(nfi)}) withpen pensquare scaled {BorderThickness};",
                $"fill ({strPositionX},{strPositionY})--({strPositionXplusWidth},{strPositionY})--({strPositionXplusWidth},{strPositionYplusHeight})--({strPositionX},{strPositionYplusHeight})--cycle withcolor({fillColorR.ToString(nfi)},{fillColorG.ToString(nfi)},{fillColorB.ToString(nfi)});",
                // Draw vertices
                $"draw ({strPositionX},{strPositionY}) withcolor({startPointFillColorR.ToString(nfi)},{startPointFillColorG.ToString(nfi)},{startPointFillColorB.ToString(nfi)}) withpen pencircle scaled {_startPoint.ShapeHeight};",
                $"label.{SpecifyPointDescPositionInMetaPost(_startPoint.DescriptionAlignment)}(btex ${_startPoint.Description}$ etex, ({strPositionX},{strPositionY}));",
                $"draw ({strPositionXplusWidth},{strPositionY}) withcolor({vertexBFillColorR.ToString(nfi)},{vertexBFillColorG.ToString(nfi)},{vertexBFillColorB.ToString(nfi)}) withpen pencircle scaled {_vertexB.ShapeHeight};",
                $"label.{SpecifyPointDescPositionInMetaPost(_vertexB.DescriptionAlignment)}(btex ${_vertexB.Description}$ etex, ({strPositionXplusWidth},{strPositionY}));",
                $"draw ({strPositionXplusWidth},{strPositionYplusHeight}) withcolor({vertexCFillColorR.ToString(nfi)},{vertexCFillColorG.ToString(nfi)},{vertexCFillColorB.ToString(nfi)}) withpen pencircle scaled {_vertexC.ShapeHeight};",
                $"label.{SpecifyPointDescPositionInMetaPost(_vertexC.DescriptionAlignment)}(btex ${_vertexC.Description}$ etex, ({strPositionXplusWidth},{strPositionYplusHeight}));",
                $"draw ({strPositionX},{strPositionYplusHeight}) withcolor({vertexDFillColorR.ToString(nfi)},{vertexDFillColorG.ToString(nfi)},{vertexDFillColorB.ToString(nfi)}) withpen pencircle scaled {_vertexD.ShapeHeight};",
                $"label.{SpecifyPointDescPositionInMetaPost(_vertexD.DescriptionAlignment)}(btex ${_vertexD.Description}$ etex, ({strPositionX},{strPositionYplusHeight}));"
            };

            // Write commands to the file before "endfig;" line.
            List<string> lines = File.ReadLines(@path).ToList();
            string lineToFind = "endfig;";
            int index = lines.IndexOf(lineToFind);
            int i = 0;
            foreach (string mpCommand in mpCommands)
            {
                lines.Insert(index + i, mpCommand);
                i++;
            }
            File.WriteAllLines(@path, lines);
        }

        #region Serialization and deserialization
        /// <summary>
        /// Serialize MpRectangle object
        /// </summary>
        public override void Serialize(XmlWriter writer)
        {
            base.Serialize(writer);
            // Start point
            writer.WriteStartElement(nameof(StartPoint));
                StartPoint.Serialize(writer);
            writer.WriteEndElement();
            // Vertex B
            writer.WriteStartElement(nameof(VertexB));
                VertexB.Serialize(writer);
            writer.WriteEndElement();
            // Vertex C
            writer.WriteStartElement(nameof(VertexC));
                VertexC.Serialize(writer);
            writer.WriteEndElement();
            // Vertex D
            writer.WriteStartElement(nameof(VertexD));
                VertexD.Serialize(writer);
            writer.WriteEndElement();
            // Shape height
            writer.WriteStartElement(nameof(ShapeHeight));
                writer.WriteString(ShapeHeight.ToString());
            writer.WriteEndElement();
            // Shape width
            writer.WriteStartElement(nameof(ShapeWidth));
                writer.WriteString(ShapeWidth.ToString());
            writer.WriteEndElement();
            // Fill color
            writer.WriteStartElement(nameof(FillColor));
                writer.WriteString(FillColor.ToString());
            writer.WriteEndElement();
            // Border color
            writer.WriteStartElement(nameof(BorderColor));
                writer.WriteString(BorderColor.ToString());
            writer.WriteEndElement();
            // Border thickness
            writer.WriteStartElement(nameof(BorderThickness));
                writer.WriteString(BorderThickness.ToString());
            writer.WriteEndElement();
        }

        /// <summary>
        /// Deserialize MpRectangle object
        /// </summary>
        public override void Deserialize(XmlReader reader)
        {
            base.Deserialize(reader);

            _startPoint = new MpPoint(_canvasCollection);
            _vertexB = new MpPoint(_canvasCollection);
            _vertexC = new MpPoint(_canvasCollection);
            _vertexD = new MpPoint(_canvasCollection);

            // Start point
            _startPoint.Deserialize(reader);
            // Vertex B
            _vertexB.Deserialize(reader);
            // Vertex C
            _vertexC.Deserialize(reader);
            // Vertex D
            _vertexD.Deserialize(reader);
            // Shape height
            _shapeHeight = reader.ReadElementContentAsInt();
            // Shape width
            _shapeWidth = reader.ReadElementContentAsInt();
            // Fill color
            _fillColor = (Color)ColorConverter.ConvertFromString(reader.ReadElementContentAsString());
            // Border color
            _borderColor = (Color)ColorConverter.ConvertFromString(reader.ReadElementContentAsString());
            // Border thickness
            _borderThickness = reader.ReadElementContentAsInt();
        }
        #endregion

        #region Data validation
        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "StartPointX":
                        if (StartPointX < 0)
                            return "The X value cannot be negative.";
                        break;
                    case "StartPointY":
                        if (StartPointY < 0)
                            return "The Y value cannot be negative.";
                        break;
                    case "ShapeHeight":
                        if (ShapeHeight < 0)
                            return "The height value cannot be negative.";
                        break;
                    case "ShapeWidth":
                        if (ShapeWidth < 0)
                            return "The width value cannot be negative.";
                        break;
                    case "BorderThickness":
                        if (BorderThickness < 0)
                            return "The border thickness value cannot be negative.";
                        break;
                }

                return string.Empty;
            }
        }

        public string Error { get; }
        #endregion
    }
}
