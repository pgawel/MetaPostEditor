﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Xml;

namespace MetaPostEditor.Model
{
    [DataContract]
    public class MpPoint : MpShape<Ellipse>
    {
        #region Properties

        private Point _position;
        public Point Position
        {
            get { return _position; }
            set
            {
                Set(ref _position, value);
                Set(ref _positionX, value.X);
                Set(ref _positionY, value.Y);
                Draw(); 
            }
        }

        private double _positionX;
        public double PositionX
        {
            get { return _positionX; }
            set
            {
                if (value < 0)
                    return;
                Set(ref _positionX, value);
                _position = new Point(value, _positionY);
            }
        }

        private double _positionY;
        public double PositionY
        {
            get { return _positionY; }
            set
            {
                if (value < 0)
                    return;
                Set(ref _positionY, value);
                _position = new Point(_positionX, value);
            }
        }

        private int _shapeHeight;
        public int ShapeHeight
        {
            get { return _shapeHeight; }
            set
            {
                if (value < 0)
                    return;
                _shapeHeight = value;
                Draw();
            }
        }

        private Color _fillColor;
        public Color FillColor
        {
            get { return _fillColor; }
            set
            {
                _fillColor = value;
                Draw();
            }
        }

        private string _description;
        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                Draw();
            }
        }

        private DefaultShapeProperties.DescriptionAllignmentType _descriptionAlignment;
        public DefaultShapeProperties.DescriptionAllignmentType DescriptionAlignment
        {
            get { return _descriptionAlignment; }
            set
            {
                _descriptionAlignment = value;
                Draw();
            }
        }

        private double _descriptionShift;

        public double DescriptionShift
        {
            get { return _descriptionShift; }
            set
            {
                _descriptionShift = value;
                Draw();
            }
        }

        private Ellipse _pointMarker;
        public Ellipse PointMarker
        {
            get { return _pointMarker; }
            private set { Set(ref _pointMarker, value); }
        }

        private TextBlock _descriptionTextBlock;

        public TextBlock DescriptionTextBlock
        {
            get { return _descriptionTextBlock; }
            set { _descriptionTextBlock = value; }
        }

        #endregion

        private ObservableCollection<object> _canvasCollection;
        private string _mpAlignment = "";

        public MpPoint()
        {
            
        }

        public MpPoint(ObservableCollection<object> canvasCollection) : base (new Ellipse())
        {
            _canvasCollection = canvasCollection;
        }

        /// <summary>
        /// Constructor that initializes object properties.
        /// </summary>
        /// <param name="coordinates"></param>
        /// <param name="canvasCollection"></param>
        public MpPoint(Point coordinates, ObservableCollection<object> canvasCollection) :this(canvasCollection)
        {
            _position = coordinates;
            _positionX = coordinates.X;
            _positionY = coordinates.Y;
            _shapeHeight = DefaultShapeProperties.GetInstance.PointHeight;
            _fillColor = DefaultShapeProperties.GetInstance.MpPointFillColor;
            _description = DefaultShapeProperties.GetInstance.PointDescription;
            _descriptionAlignment = DefaultShapeProperties.GetInstance.DescriptionAlignment;
            _descriptionShift = DefaultShapeProperties.GetInstance.DescriptionShift;
        }

        public MpPoint(int id, Point coordinates, ObservableCollection<object> canvasCollection)
            : base(id, "Point" + id, new Ellipse())
        {
            _canvasCollection = canvasCollection;
            _position = coordinates;
            _positionX = coordinates.X;
            _positionY = coordinates.Y;
            _shapeHeight = DefaultShapeProperties.GetInstance.PointHeight;
            _fillColor = DefaultShapeProperties.GetInstance.FillColor;
            _description = DefaultShapeProperties.GetInstance.PointDescription;
            _descriptionAlignment = DefaultShapeProperties.GetInstance.DescriptionAlignment;
            _descriptionShift = DefaultShapeProperties.GetInstance.DescriptionShift;
        }

        public override void InitializeForDrawing(ObservableCollection<object> canvasCollection)
        {
            _canvasCollection = canvasCollection;
        }

        /// <summary>
        /// Draws current object on canvas.
        /// </summary>
        public override void Draw()
        {
            // draw point
            if (_pointMarker == null)
            {
                _pointMarker = new Ellipse();
                _canvasCollection.Add(_pointMarker);
            }

            SetProperties();

            // set description
            double coordinateX = _positionX;
            double coordinateY = _positionY;

            if (_descriptionTextBlock == null)
            {
                _descriptionTextBlock = new TextBlock();
                _canvasCollection.Add(_descriptionTextBlock);
            }

            _descriptionTextBlock.Text = Description;
            _descriptionTextBlock.TextAlignment = TextAlignment.Right;
            _descriptionTextBlock.TextWrapping = TextWrapping.Wrap;

            switch (DescriptionAlignment)
            {
                case DefaultShapeProperties.DescriptionAllignmentType.Top:
                    coordinateY -= _descriptionShift;
                    _mpAlignment = "top";
                    break;
                case DefaultShapeProperties.DescriptionAllignmentType.Right:
                    coordinateX += _descriptionShift;
                    _mpAlignment = "rt";
                    break;
                case DefaultShapeProperties.DescriptionAllignmentType.Bottom:
                    coordinateY += _descriptionShift;
                    _mpAlignment = "bot";
                    break;
                case DefaultShapeProperties.DescriptionAllignmentType.Left:
                    coordinateX -= _descriptionShift;
                    _mpAlignment = "lft";
                    break;
                case DefaultShapeProperties.DescriptionAllignmentType.TopRight:
                    coordinateX += _descriptionShift;
                    coordinateY -= _descriptionShift;
                    _mpAlignment = "urt";
                    break;
                case DefaultShapeProperties.DescriptionAllignmentType.BottomRight:
                    coordinateX += _descriptionShift;
                    coordinateY += _descriptionShift;
                    _mpAlignment = "lrt";
                    break;
                case DefaultShapeProperties.DescriptionAllignmentType.BottomLeft:
                    coordinateX -= _descriptionShift;
                    coordinateY += _descriptionShift;
                    _mpAlignment = "llft";
                    break;
                case DefaultShapeProperties.DescriptionAllignmentType.TopLeft:
                    coordinateX -= _descriptionShift;
                    coordinateY -= _descriptionShift;
                    _mpAlignment = "ulft";
                    break;
            }

            // place the text block in specified position on canvasCollection
            _descriptionTextBlock.Measure(new Size(Double.PositiveInfinity, Double.PositiveInfinity));
            _descriptionTextBlock.Arrange(new Rect(_descriptionTextBlock.DesiredSize));
            _descriptionTextBlock.Margin = new Thickness(coordinateX - (_descriptionTextBlock.ActualWidth / 2), coordinateY - (_descriptionTextBlock.ActualHeight / 2), 0, 0);
        }

        /// <summary>
        /// Remove current object from canvas.
        /// </summary>
        public void Remove()
        {
            _canvasCollection.Remove(_descriptionTextBlock);
            _canvasCollection.Remove(_pointMarker);
        }

        /// <summary>
        /// Sets current shape properties.
        /// </summary>
        private void SetProperties()
        {
            _pointMarker.Margin = new Thickness(_positionX - (ShapeHeight / 2.0), _positionY - (ShapeHeight / 2.0), 0, 0);
            _pointMarker.Height = ShapeHeight;
            _pointMarker.Width = ShapeHeight;
            _pointMarker.Fill = new SolidColorBrush(FillColor);
            _pointMarker.StrokeThickness = 0;
        }

        /// <summary>
        /// Return current shape position
        /// </summary>
        /// <returns>Current shape position</returns>
        public override Point GetPosition()
        {
            return Position;
        }

        /// <summary>
        /// Move current object to given location.
        /// </summary>
        /// <param name="point"></param>
        public override void MoveTo(Point point, double canvasHeight, double canvasWidth)
        {
            PositionX = point.X;
            PositionY = point.Y;
        }

        /// <summary>
        /// Converts current object into MetaPost language and saves it in a file.
        /// </summary>
        public override void ConvertIntoMetaPost(string path)
        {
            NumberFormatInfo nfi = new NumberFormatInfo {NumberDecimalSeparator = "."};

            // TODO yMax cannot be const value, set yMax programmatically
            double yMax = 842;

            // coordinates
            string strPositionX = Convert.ToString(Position.X);
            string strPositionY = Convert.ToString(yMax - Position.Y);

            // Fill color in RGB
            double fillColorR = Math.Round(FillColor.R / 255.0, 3);
            double fillColorG = Math.Round(FillColor.G / 255.0, 3);
            double fillColorB = Math.Round(FillColor.B / 255.0, 3);

            List<string> mpCommands = new List<string>()
            {
                "% --- --- Point ---",
                $"draw ({strPositionX},{strPositionY}) withcolor({fillColorR.ToString(nfi)},{fillColorG.ToString(nfi)},{fillColorB.ToString(nfi)}) withpen pencircle scaled {ShapeHeight};"
            };

            if (_mpAlignment != "")
            {
                mpCommands.Add($"label.{_mpAlignment}(btex ${Description}$ etex, ({strPositionX},{strPositionY}));");
            }

            // Write commands to the file before "endfig;" line.
            List<string> lines = File.ReadLines(@path).ToList();
            string lineToFind = "endfig;";
            int index = lines.IndexOf(lineToFind);
            int i = 0;
            foreach (string mpCommand in mpCommands.ToList())
            {
                lines.Insert(index + i, mpCommand);
                i++;
            }
            File.WriteAllLines(@path, lines);
        }

        #region Data validation
        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "PositionX":
                        if (PositionX < 0)
                            return "The X value cannot be negative.";
                        break;
                    case "PositionY":
                        if (PositionY < 0)
                            return "The Y value cannot be negative.";
                        break;
                    case "ShapeHeight":
                        if (ShapeHeight < 0)
                            return "The height value cannot be negative.";
                        break;
                }

                return string.Empty;
            }
        }

        public string Error { get; }
        #endregion

        #region Serialization and deserialization
        public override void Serialize(XmlWriter writer)
        {
            base.Serialize(writer);
            writer.WriteStartElement(nameof(PositionX));
                writer.WriteString(PositionX.ToString());
            writer.WriteEndElement();
            writer.WriteStartElement(nameof(PositionY));
                writer.WriteString(PositionY.ToString());
            writer.WriteEndElement();
            writer.WriteStartElement(nameof(ShapeHeight));
                writer.WriteString(ShapeHeight.ToString());
            writer.WriteEndElement();
            writer.WriteStartElement(nameof(FillColor));
                writer.WriteString(FillColor.ToString());
            writer.WriteEndElement();
            writer.WriteStartElement(nameof(Description));
                writer.WriteString(Description);
            writer.WriteEndElement();
            writer.WriteStartElement(nameof(DescriptionAlignment));
                writer.WriteString(DescriptionAlignment.ToString());
            writer.WriteEndElement();
            writer.WriteStartElement(nameof(DescriptionShift));
                writer.WriteString(DescriptionShift.ToString());
            writer.WriteEndElement();
        }

        public override void Deserialize(XmlReader reader)
        {
            base.Deserialize(reader);
            PositionX = reader.ReadElementContentAsDouble();
            PositionY = reader.ReadElementContentAsDouble();
            _shapeHeight = reader.ReadElementContentAsInt();
            _fillColor = (Color)ColorConverter.ConvertFromString(reader.ReadElementContentAsString());
            _description = reader.ReadElementContentAsString();
            _descriptionAlignment = (DefaultShapeProperties.DescriptionAllignmentType)Enum.Parse(typeof(DefaultShapeProperties.DescriptionAllignmentType), reader.ReadElementContentAsString());
            _descriptionShift = reader.ReadElementContentAsInt();
            reader.Read();
        }
        #endregion
    }
}
