﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Xml;

namespace MetaPostEditor.Model
{
    [DataContract]
    public class MpEllipse : MpShape<Ellipse>, IDataErrorInfo
    {
        #region Properties

        private MpPoint _startPoint;
        public MpPoint StartPoint
        {
            get { return _startPoint; }
            set
            {
                Set(ref _startPoint, value);
                Draw();
            }
        }

        private double _startPointX;
        public double StartPointX
        {
            get { return _startPointX; }
            set
            {
                if (value < 0)
                    return;
                var oldStartPoint = StartPoint;
                _startPoint.Remove();
                Set(ref _startPointX, value);
                StartPoint = new MpPoint(new Point(value, _startPointY), _canvasCollection)
                {
                    Description = oldStartPoint.Description,
                    DescriptionAlignment = oldStartPoint.DescriptionAlignment,
                    FillColor = oldStartPoint.FillColor
                };
            }
        }

        private double _startPointY;
        public double StartPointY
        {
            get { return _startPointY; }
            set
            {
                if (value < 0)
                    return;
                var oldStartPoint = StartPoint;
                _startPoint.Remove();
                Set(ref _startPointY, value);
                StartPoint = new MpPoint(new Point(_startPointX, value), _canvasCollection)
                {
                    Description = oldStartPoint.Description,
                    DescriptionAlignment = oldStartPoint.DescriptionAlignment,
                    FillColor = oldStartPoint.FillColor
                };
            }
        }

        private int _shapeHeight;
        public int ShapeHeight
        {
            get { return _shapeHeight; }
            set
            {
                if (value < 0)
                    return;
                _shapeHeight = value;
                Draw();
            }
        }

        private int _shapeWidth;
        public int ShapeWidth
        {
            get { return _shapeWidth; }
            set
            {
                if (value < 0)
                    return;
                _shapeWidth = value;
                Draw();
            }
        }

        private Color _fillColor;
        public Color FillColor
        {
            get { return _fillColor; }
            set
            {
                _fillColor = value;
                Draw();
            }
        }

        private Color _borderColor;
        public Color BorderColor
        {
            get { return _borderColor; }
            set
            {
                _borderColor = value;
                Draw();
            }
        }

        private int _borderThickness;
        public int BorderThickness
        {
            get { return _borderThickness; }
            set
            {
                if (value < 0)
                    return;
                _borderThickness = value;
                Draw();
            }
        }

        #endregion

        private ObservableCollection<object> _canvasCollection;
        private bool _isAlreadyOnCanvas;

        /// <summary>
        /// Empty contructor
        /// </summary>
        public MpEllipse() { }
        
        /// <summary>
        /// Constructor that initializes object properties.
        /// </summary>
        /// <param name="id">Shape identification number.</param>
        /// <param name="canvasCollection"></param>
        public MpEllipse(int id, ObservableCollection<object> canvasCollection) 
            : base(id, "Ellipse" + id, new Ellipse())
        {
            _canvasCollection = canvasCollection;
            _startPoint = new MpPoint(DefaultShapeProperties.GetInstance.StartPoint, _canvasCollection);
            _startPointX = DefaultShapeProperties.GetInstance.StartPoint.X;
            _startPointY = DefaultShapeProperties.GetInstance.StartPoint.Y;
            _shapeHeight = DefaultShapeProperties.GetInstance.ShapeHeight;
            _shapeWidth = DefaultShapeProperties.GetInstance.ShapeWidth;
            _fillColor = DefaultShapeProperties.GetInstance.FillColor;
            _borderColor = DefaultShapeProperties.GetInstance.BorderColor;
            _borderThickness = DefaultShapeProperties.GetInstance.BorderThickness;

            _canvasCollection.Add(ShapeInstance);
            _isAlreadyOnCanvas = true;
        }

        public override void InitializeForDrawing(ObservableCollection<object> canvasCollection)
        {
            _canvasCollection = canvasCollection;
            StartPoint.InitializeForDrawing(canvasCollection);
        }

        /// <summary>
        /// Draws current object.
        /// </summary>
        public override void Draw()
        {
            SetProperties();
            _startPoint.Draw();

            if (_isAlreadyOnCanvas) return;
            _canvasCollection.Add(ShapeInstance);
            _isAlreadyOnCanvas = true;
        }

        /// <summary>
        /// Sets current shape properties.
        /// </summary>
        private void SetProperties()
        {
            ShapeInstance.Margin = new Thickness(_startPoint.PositionX - (ShapeWidth / 2.0), _startPoint.PositionY - (ShapeHeight / 2.0), 0, 0);
            ShapeInstance.Height = ShapeHeight;
            ShapeInstance.Width = ShapeWidth;
            ShapeInstance.Fill = new SolidColorBrush(FillColor);
            ShapeInstance.Stroke = new SolidColorBrush(BorderColor);
            ShapeInstance.StrokeThickness = BorderThickness;
        }

        /// <summary>
        /// Return current shape position (midpoint)
        /// </summary>
        /// <returns>Current shape position</returns>
        public override Point GetPosition()
        {
            return StartPoint.Position;
        }

        /// <summary>
        /// Move current object to given location.
        /// </summary>
        /// <param name="point"></param>
        /// <param name="canvasHeight"></param>
        /// <param name="canvasWidth"></param>
        public override void MoveTo(Point point, double canvasHeight, double canvasWidth)
        {
            var shift = _startPoint.DescriptionShift;
            var horizontalRadius = _shapeWidth / 2.0;
            var verticalRadius = _shapeHeight / 2.0;

            if (point.X + horizontalRadius + shift > canvasWidth || point.Y + verticalRadius + shift > canvasHeight || point.X - horizontalRadius - shift < 0 || point.Y - verticalRadius - shift < 0)
            {
                return;
            }

            StartPointX = point.X;
            StartPointY = point.Y;
        }

        /// <summary>
        /// Converts current object into MetaPost language and saves it in a file.
        /// </summary>
        public override void ConvertIntoMetaPost(string path)
        {
            NumberFormatInfo nfi = new NumberFormatInfo {NumberDecimalSeparator = "."};
            double yMax = 842;

            // Ellipse coordinates
            string strPositionX = Convert.ToString(StartPoint.PositionX);
            string strPositionY = Convert.ToString(yMax - StartPoint.PositionY);
            string strPositionXRight = Convert.ToString(StartPoint.PositionX + (ShapeWidth / 2.0));
            string strPositionXLeft = Convert.ToString(StartPoint.PositionX - (ShapeWidth / 2.0));
            string strPositionYTop = Convert.ToString(yMax - StartPoint.PositionY - (ShapeHeight / 2.0));
            string strPositionYBottom = Convert.ToString(yMax - StartPoint.PositionY + (ShapeHeight / 2.0));

            // Border color in RGB
            double borderColorR = Math.Round(BorderColor.R / 255.0, 3);
            double borderColorG = Math.Round(BorderColor.G / 255.0, 3);
            double borderColorB = Math.Round(BorderColor.B / 255.0, 3);

            // Fill color in RGB
            double fillColorR = Math.Round(FillColor.R / 255.0, 3);
            double fillColorG = Math.Round(FillColor.G / 255.0, 3);
            double fillColorB = Math.Round(FillColor.B / 255.0, 3);

            // Start point fill color in RGB
            double startPointFillColorR = Math.Round(_startPoint.FillColor.R / 255.0, 3);
            double startPointFillColorG = Math.Round(_startPoint.FillColor.G / 255.0, 3);
            double startPointFillColorB = Math.Round(_startPoint.FillColor.B / 255.0, 3);

            string[] mpCommands =
            {
                $"% --- Ellipse: Shape id: {Id} | Name: {Name} ---",
                // Draw ellipse
                $"draw ({strPositionX},{strPositionYTop})..({strPositionXRight},{strPositionY})..({strPositionX},{strPositionYBottom})..({strPositionXLeft},{strPositionY})..cycle withcolor({borderColorR.ToString(nfi)},{borderColorG.ToString(nfi)},{borderColorB.ToString(nfi)}) withpen pensquare scaled {BorderThickness};",
                $"fill ({strPositionX},{strPositionYTop})..({strPositionXRight},{strPositionY})..({strPositionX},{strPositionYBottom})..({strPositionXLeft},{strPositionY})..cycle withcolor({fillColorR.ToString(nfi)},{fillColorG.ToString(nfi)},{fillColorB.ToString(nfi)});",
                // Draw midpoint                
                $"draw ({strPositionX},{strPositionY}) withcolor({startPointFillColorR.ToString(nfi)},{startPointFillColorG.ToString(nfi)},{startPointFillColorB.ToString(nfi)}) withpen pencircle scaled {_startPoint.ShapeHeight};",
                $"label.{SpecifyPointDescPositionInMetaPost(_startPoint.DescriptionAlignment)}(btex ${_startPoint.Description}$ etex, ({strPositionX},{strPositionY}));"
            };

            // Write commands to the file before "endfig;" line.
            List<string> lines = File.ReadLines(@path).ToList();
            string lineToFind = "endfig;";
            int index = lines.IndexOf(lineToFind);
            int i = 0;
            foreach (string mpCommand in mpCommands)
            {
                lines.Insert(index + i, mpCommand);
                i++;
            }
            File.WriteAllLines(@path, lines);
        }

        #region Data validation
        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "StartPointX":
                        if (StartPointX < 0)
                            return "The X value cannot be negative.";
                        break;
                    case "StartPointY":
                        if (StartPointY < 0)
                            return "The Y value cannot be negative.";
                        break;
                    case "ShapeHeight":
                        if (ShapeHeight < 0)
                            return "The height value cannot be negative.";
                        break;
                    case "ShapeWidth":
                        if (ShapeWidth < 0)
                            return "The width value cannot be negative.";
                        break;
                    case "BorderThickness":
                        if (BorderThickness < 0)
                            return "The border thickness value cannot be negative.";
                        break;
                }

                return string.Empty;
            }
        }

        public string Error { get; }
        #endregion

        #region Serialization and deserialization
        /// <summary>
        /// Serialize MpEllipse object
        /// </summary>
        /// <param name="writer"></param>
        public override void Serialize(XmlWriter writer)
        {
            base.Serialize(writer);
            // Start point (Midpoint)
            writer.WriteStartElement(nameof(StartPoint));
                StartPoint.Serialize(writer);
            writer.WriteEndElement();
            // Shape height
            writer.WriteStartElement(nameof(ShapeHeight));
                writer.WriteString(ShapeHeight.ToString());
            writer.WriteEndElement();
            // Shape width
            writer.WriteStartElement(nameof(ShapeWidth));
                writer.WriteString(ShapeWidth.ToString());
            writer.WriteEndElement();
            // Fill color
            writer.WriteStartElement(nameof(FillColor));
                writer.WriteString(FillColor.ToString());
            writer.WriteEndElement();
            // Border color
            writer.WriteStartElement(nameof(BorderColor));
                writer.WriteString(BorderColor.ToString());
            writer.WriteEndElement();
            // Border thickness
            writer.WriteStartElement(nameof(BorderThickness));
                writer.WriteString(BorderThickness.ToString());
            writer.WriteEndElement();
        }

        /// <summary>
        /// Deserialize MpEllipse object
        /// </summary>
        public override void Deserialize(XmlReader reader)
        {
            base.Deserialize(reader);

            _startPoint = new MpPoint(_canvasCollection);

            // Start point
            _startPoint.Deserialize(reader);
            // Shape height
            _shapeHeight = reader.ReadElementContentAsInt();
            // Shape width
            _shapeWidth = reader.ReadElementContentAsInt();
            // Fill color
            _fillColor = (Color)ColorConverter.ConvertFromString(reader.ReadElementContentAsString());
            // Border color
            _borderColor = (Color)ColorConverter.ConvertFromString(reader.ReadElementContentAsString());
            // Border thickness
            _borderThickness = reader.ReadElementContentAsInt();
        }
        #endregion
    }
}