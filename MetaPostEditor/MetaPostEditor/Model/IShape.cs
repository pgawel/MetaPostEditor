﻿using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using System.Windows.Input;
using System.Windows.Shapes;
using System.Xml;

namespace MetaPostEditor.Model
{
    public interface IMpShape<out TBaseShape> : IShape
    {
    }

    public interface IShape
    {
        Shape ShapeGeneral { get; }

        event MouseEventHandler MouseEnter;
        event MouseButtonEventHandler MouseLeftButtonDown;
        event MouseEventHandler MouseMove;
        event MouseButtonEventHandler MouseUp;

        void MoveTo(Point point, double canvasHeight, double canvasWidth);

        void InitializeForDrawing(ObservableCollection<object> canvasCollection);

        void Draw();

        Point GetPosition();

        void ConvertIntoMetaPost(string path);

        void Serialize(XmlWriter writer);

        void Deserialize(XmlReader reader);
    }
}