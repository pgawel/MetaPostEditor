﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Xml;

namespace MetaPostEditor.Model
{
    [DataContract]
    public class MpPolygon : MpShape<Polygon>
    {
        #region Properties

        #region Vertex A properties
        private MpPoint _vertexA;
        public MpPoint VertexA
        {
            get { return _vertexA; }
            set
            {
                Set(ref _vertexA, value);
                Draw();
            }
        }

        private double _vertexAx;
        public double VertexAx
        {
            get { return _vertexAx; }
            set
            {
                if (value < 0)
                    return;
                _vertexA.Remove();
                Set(ref _vertexAx, value);
                var oldVertexA = VertexA;
                VertexA = new MpPoint(new Point(value, _vertexAy), _canvasCollection)
                {
                    Description = oldVertexA.Description,
                    DescriptionAlignment = oldVertexA.DescriptionAlignment,
                    FillColor = oldVertexA.FillColor
                };
            }
        }

        private double _vertexAy;
        public double VertexAy
        {
            get { return _vertexAy; }
            set
            {
                if (value < 0)
                    return;
                _vertexA.Remove();
                Set(ref _vertexAy, value);
                var oldVertexA = VertexA;
                VertexA = new MpPoint(new Point(_vertexAx, value), _canvasCollection)
                {
                    Description = oldVertexA.Description,
                    DescriptionAlignment = oldVertexA.DescriptionAlignment,
                    FillColor = oldVertexA.FillColor
                };
            }
        }
        #endregion

        #region Vertex B properties
        private MpPoint _vertexB;
        public MpPoint VertexB
        {
            get { return _vertexB; }
            set
            {
                Set(ref _vertexB, value);
                Draw();
            }
        }

        private double _vertexBx;
        public double VertexBx
        {
            get { return _vertexBx; }
            set
            {
                if (value < 0)
                    return;
                _vertexB.Remove();
                Set(ref _vertexBx, value);
                var oldVertexB = VertexB;
                VertexB = new MpPoint(new Point(value, _vertexBy), _canvasCollection)
                {
                    Description = oldVertexB.Description,
                    DescriptionAlignment = oldVertexB.DescriptionAlignment,
                    FillColor = oldVertexB.FillColor
                };
            }
        }

        private double _vertexBy;
        public double VertexBy
        {
            get { return _vertexBy; }
            set
            {
                if (value < 0)
                    return;
                _vertexB.Remove();
                Set(ref _vertexBy, value);
                var oldVertexB = VertexB;
                VertexB = new MpPoint(new Point(_vertexBx, value), _canvasCollection)
                {
                    Description = oldVertexB.Description,
                    DescriptionAlignment = oldVertexB.DescriptionAlignment,
                    FillColor = oldVertexB.FillColor
                };
            }
        }
        #endregion

        #region Vertex C properties
        private MpPoint _vertexC;
        public MpPoint VertexC
        {
            get { return _vertexC; }
            set
            {
                Set(ref _vertexC, value);
                Draw();
            }
        }

        private double _vertexCx;
        public double VertexCx
        {
            get { return _vertexCx; }
            set
            {
                if (value < 0)
                    return;
                _vertexC.Remove();
                Set(ref _vertexCx, value);
                var oldVertexC = VertexC;
                VertexC = new MpPoint(new Point(value, _vertexCy), _canvasCollection)
                {
                    Description = oldVertexC.Description,
                    DescriptionAlignment = oldVertexC.DescriptionAlignment,
                    FillColor = oldVertexC.FillColor
                };
            }
        }

        private double _vertexCy;
        public double VertexCy
        {
            get { return _vertexCy; }
            set
            {
                if (value < 0)
                    return;
                _vertexC.Remove();
                Set(ref _vertexCy, value);
                var oldVertexC = VertexC;
                VertexC = new MpPoint(new Point(_vertexCx, value), _canvasCollection)
                {
                    Description = oldVertexC.Description,
                    DescriptionAlignment = oldVertexC.DescriptionAlignment,
                    FillColor = oldVertexC.FillColor
                };
            }
        }
        #endregion

        private Color _fillColor;
        public Color FillColor
        {
            get { return _fillColor; }
            set
            {
                _fillColor = value;
                Draw();
            }
        }

        private Color _borderColor;
        public Color BorderColor
        {
            get { return _borderColor; }
            set
            {
                _borderColor = value;
                Draw();
            }
        }

        private int _borderThickness;
        public int BorderThickness
        {
            get { return _borderThickness; }
            set
            {
                if (value < 0)
                    return;
                _borderThickness = value;
                Draw();
            }
        }

        #endregion

        private ObservableCollection<object> _canvasCollection;
        private PointCollection _verticesPointsCollection;
        private bool _isAlreadyOnCanvas;

        /// <summary>
        /// Empty contstructor
        /// </summary>
        public MpPolygon() { }

        /// <summary>
        /// Constructor that initializes object properties.
        /// </summary>
        /// <param name="id">Shape identification number.</param>
        /// <param name="canvasCollection"></param>
        public MpPolygon(int id, ObservableCollection<object> canvasCollection) 
            : base(id, "Polygon" + id, new Polygon())
        {
            _canvasCollection = canvasCollection;
            _vertexA = new MpPoint(DefaultShapeProperties.GetInstance.VertexA, _canvasCollection);
            _vertexAx = DefaultShapeProperties.GetInstance.VertexA.X;
            _vertexAy = DefaultShapeProperties.GetInstance.VertexA.Y;
            _vertexB = new MpPoint(DefaultShapeProperties.GetInstance.VertexB, _canvasCollection)
            {
                Description = "B",
                DescriptionAlignment = DefaultShapeProperties.DescriptionAllignmentType.Left
            };
            _vertexBx = DefaultShapeProperties.GetInstance.VertexB.X;
            _vertexBy = DefaultShapeProperties.GetInstance.VertexB.Y;
            _vertexC = new MpPoint(DefaultShapeProperties.GetInstance.VertexC, _canvasCollection)
            {
                Description = "C",
                DescriptionAlignment = DefaultShapeProperties.DescriptionAllignmentType.Right
            };
            _vertexCx = DefaultShapeProperties.GetInstance.VertexC.X;
            _vertexCy = DefaultShapeProperties.GetInstance.VertexC.Y;
            _fillColor = DefaultShapeProperties.GetInstance.FillColor;
            _borderColor = DefaultShapeProperties.GetInstance.BorderColor;
            _borderThickness = DefaultShapeProperties.GetInstance.BorderThickness;

            _canvasCollection.Add(ShapeInstance);
            _isAlreadyOnCanvas = true;
        }

        public override void InitializeForDrawing(ObservableCollection<object> canvasCollection)
        {
            _canvasCollection = canvasCollection;
            VertexA.InitializeForDrawing(canvasCollection);
            VertexB.InitializeForDrawing(canvasCollection);
            VertexC.InitializeForDrawing(canvasCollection);
        }

        /// <summary>
        /// Draws current object.
        /// </summary>
        public override void Draw()
        {
            SetProperties();
            _vertexA.Draw();
            _vertexB.Draw();
            _vertexC.Draw();

            if (_isAlreadyOnCanvas) return;
            _canvasCollection.Add(ShapeInstance);
            _isAlreadyOnCanvas = true;
        }

        /// <summary>
        /// Sets current shape properties.
        /// </summary>
        private void SetProperties()
        {
            _verticesPointsCollection = new PointCollection {_vertexA.Position, _vertexB.Position, _vertexC.Position};
            ShapeInstance.Points = _verticesPointsCollection;
            ShapeInstance.Fill = new SolidColorBrush(_fillColor);
            ShapeInstance.Stroke = new SolidColorBrush(_borderColor);
            ShapeInstance.StrokeThickness = _borderThickness;
        }

        /// <summary>
        /// Return current shape position (first vertex)
        /// </summary>
        /// <returns>Current shape position</returns>
        public override Point GetPosition()
        {
            return VertexA.Position;
        }

        /// <summary>
        /// Move current object to given location.
        /// </summary>
        /// <param name="point"></param>
        /// <param name="canvasHeight"></param>
        /// <param name="canvasWidth"></param>
        public override void MoveTo(Point point, double canvasHeight, double canvasWidth)
        {
            var tmpVertexBx = point.X + (_vertexB.Position.X - _vertexA.Position.X);
            var tmpVertexBy = point.Y + (_vertexB.Position.Y - _vertexA.Position.Y);
            var tmpVertexCx = point.X + (_vertexC.Position.X - _vertexA.Position.X);
            var tmpVertexCy = point.Y + (_vertexC.Position.Y - _vertexA.Position.Y);

            var shift = _vertexA.DescriptionShift;

            if (point.X + shift > canvasWidth || point.Y + shift > canvasHeight || point.X - shift < 0 || point.Y - shift < 0 ||
                tmpVertexBx + shift > canvasWidth || tmpVertexBy + shift > canvasHeight || tmpVertexBx - shift < 0 || tmpVertexBy - shift < 0 ||
                tmpVertexCx + shift > canvasWidth || tmpVertexCy + shift > canvasHeight || tmpVertexCx - shift < 0 || tmpVertexCy - shift < 0)
            {
                return;
            }

            VertexAx = point.X;
            VertexAy = point.Y;
            VertexBx = tmpVertexBx;
            VertexBy = tmpVertexBy;
            VertexCx = tmpVertexCx;
            VertexCy = tmpVertexCy;
        }

        /// <summary>
        /// Converts current object into MetaPost language and saves it in a file.
        /// </summary>
        public override void ConvertIntoMetaPost(string path)
        {
            NumberFormatInfo nfi = new NumberFormatInfo {NumberDecimalSeparator = "."};

            // TODO yMax cannot be const value, set yMax programmatically
            double yMax = 842;

            // Coordinates
            string strPositionVertexAx = Convert.ToString(_vertexA.Position.X);
            string strPositionVertexAy = Convert.ToString(yMax - _vertexA.Position.Y);
            string strPositionVertexBx = Convert.ToString(_vertexB.Position.X);
            string strPositionVertexBy = Convert.ToString(yMax - _vertexB.Position.Y);
            string strPositionVertexCx = Convert.ToString(_vertexC.Position.X);
            string strPositionVertexCy = Convert.ToString(yMax - _vertexC.Position.Y);

            // Border color in RGB
            double borderColorR = Math.Round(BorderColor.R / 255.0, 3);
            double borderColorG = Math.Round(BorderColor.G / 255.0, 3);
            double borderColorB = Math.Round(BorderColor.B / 255.0, 3);

            // Fill color in RGB
            double fillColorR = Math.Round(FillColor.R / 255.0, 3);
            double fillColorG = Math.Round(FillColor.G / 255.0, 3);
            double fillColorB = Math.Round(FillColor.B / 255.0, 3);

            // Vertex A fill color in RGB
            double vertexAFillColorR = Math.Round(_vertexA.FillColor.R / 255.0, 3);
            double vertexAFillColorG = Math.Round(_vertexA.FillColor.G / 255.0, 3);
            double vertexAFillColorB = Math.Round(_vertexA.FillColor.B / 255.0, 3);

            // Vertex B fill color in RGB
            double vertexBFillColorR = Math.Round(_vertexB.FillColor.R / 255.0, 3);
            double vertexBFillColorG = Math.Round(_vertexB.FillColor.G / 255.0, 3);
            double vertexBFillColorB = Math.Round(_vertexB.FillColor.B / 255.0, 3);

            // Vertex C fill color in RGB
            double vertexCFillColorR = Math.Round(_vertexC.FillColor.R / 255.0, 3);
            double vertexCFillColorG = Math.Round(_vertexC.FillColor.G / 255.0, 3);
            double vertexCFillColorB = Math.Round(_vertexC.FillColor.B / 255.0, 3);

            if (_verticesPointsCollection != null)
            {
                string[] mpCommands =
                {
                    $"% --- Polygon: Shape id: {Id} | Name: {Name} ---",
                    // Draw rectangle
                    $"draw ({strPositionVertexAx},{strPositionVertexAy})--({strPositionVertexBx},{strPositionVertexBy})--({strPositionVertexCx},{strPositionVertexCy})--cycle withcolor({borderColorR.ToString(nfi)},{borderColorG.ToString(nfi)},{borderColorB.ToString(nfi)}) withpen pensquare scaled {BorderThickness};",
                    $"fill ({strPositionVertexAx},{strPositionVertexAy})--({strPositionVertexBx},{strPositionVertexBy})--({strPositionVertexCx},{strPositionVertexCy})--cycle withcolor({fillColorR.ToString(nfi)},{fillColorG.ToString(nfi)},{fillColorB.ToString(nfi)});",
                    // Draw vertices
                    $"draw ({strPositionVertexAx},{strPositionVertexAy}) withcolor({vertexAFillColorR.ToString(nfi)},{vertexAFillColorG.ToString(nfi)},{vertexAFillColorB.ToString(nfi)}) withpen pencircle scaled {_vertexA.ShapeHeight};",
                    $"label.{SpecifyPointDescPositionInMetaPost(_vertexA.DescriptionAlignment)}(btex ${_vertexA.Description}$ etex, ({strPositionVertexAx},{strPositionVertexAy}));",
                    $"draw ({strPositionVertexBx},{strPositionVertexBy}) withcolor({vertexBFillColorR.ToString(nfi)},{vertexBFillColorG.ToString(nfi)},{vertexBFillColorB.ToString(nfi)}) withpen pencircle scaled {_vertexB.ShapeHeight};",
                    $"label.{SpecifyPointDescPositionInMetaPost(_vertexB.DescriptionAlignment)}(btex ${_vertexB.Description}$ etex, ({strPositionVertexBx},{strPositionVertexBy}));",
                    $"draw ({strPositionVertexCx},{strPositionVertexCy}) withcolor({vertexCFillColorR.ToString(nfi)},{vertexCFillColorG.ToString(nfi)},{vertexCFillColorB.ToString(nfi)}) withpen pencircle scaled {_vertexC.ShapeHeight};",
                    $"label.{SpecifyPointDescPositionInMetaPost(_vertexC.DescriptionAlignment)}(btex ${_vertexC.Description}$ etex, ({strPositionVertexCx},{strPositionVertexCy}));",
                };

                // Write commands to the file before "endfig;" line.
                List<string> lines = File.ReadLines(@path).ToList();
                string lineToFind = "endfig;";
                int index = lines.IndexOf(lineToFind);
                int i = 0;
                foreach (string mpCommand in mpCommands)
                {
                    lines.Insert(index + i, mpCommand);
                    i++;
                }
                File.WriteAllLines(@path, lines);
            }
        }

        #region Data validation
        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "VertexAx":
                        if (VertexAx < 0)
                            return "The X value cannot be negative.";
                        break;
                    case "VertexAy":
                        if (VertexAy < 0)
                            return "The Y value cannot be negative.";
                        break;
                    case "VertexBx":
                        if (VertexBx < 0)
                            return "The X value cannot be negative.";
                        break;
                    case "VertexBy":
                        if (VertexBy < 0)
                            return "The Y value cannot be negative.";
                        break;
                    case "VertexCx":
                        if (VertexCx < 0)
                            return "The X value cannot be negative.";
                        break;
                    case "VertexCy":
                        if (VertexCy < 0)
                            return "The Y value cannot be negative.";
                        break;
                    case "BorderThickness":
                        if (BorderThickness < 0)
                            return "The border thickness value cannot be negative.";
                        break;
                }

                return string.Empty;
            }
        }

        public string Error { get; }
        #endregion

        #region Serialization and deserialization
        /// <summary>
        /// Serialize MpPolygon object
        /// </summary>
        public override void Serialize(XmlWriter writer)
        {
            base.Serialize(writer);
            // Start point
            writer.WriteStartElement(nameof(VertexA));
            VertexA.Serialize(writer);
            writer.WriteEndElement();
            // Vertex B
            writer.WriteStartElement(nameof(VertexB));
            VertexB.Serialize(writer);
            writer.WriteEndElement();
            // Vertex C
            writer.WriteStartElement(nameof(VertexC));
            VertexC.Serialize(writer);
            writer.WriteEndElement();
            // Fill color
            writer.WriteStartElement(nameof(FillColor));
            writer.WriteString(FillColor.ToString());
            writer.WriteEndElement();
            // Border color
            writer.WriteStartElement(nameof(BorderColor));
            writer.WriteString(BorderColor.ToString());
            writer.WriteEndElement();
            // Border thickness
            writer.WriteStartElement(nameof(BorderThickness));
            writer.WriteString(BorderThickness.ToString());
            writer.WriteEndElement();
        }

        /// <summary>
        /// Deserialize MpPolygon object
        /// </summary>
        public override void Deserialize(XmlReader reader)
        {
            base.Deserialize(reader);

            _vertexA = new MpPoint(_canvasCollection);
            _vertexB = new MpPoint(_canvasCollection);
            _vertexC = new MpPoint(_canvasCollection);

            // Vertex A
            _vertexA.Deserialize(reader);
            // Vertex B
            _vertexB.Deserialize(reader);
            // Vertex C
            _vertexC.Deserialize(reader);
            // Fill color
            _fillColor = (Color)ColorConverter.ConvertFromString(reader.ReadElementContentAsString());
            // Border color
            _borderColor = (Color)ColorConverter.ConvertFromString(reader.ReadElementContentAsString());
            // Border thickness
            _borderThickness = reader.ReadElementContentAsInt();
        }
        #endregion
    }
}
