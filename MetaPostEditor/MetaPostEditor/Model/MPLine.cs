﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Xml;

namespace MetaPostEditor.Model
{
    [DataContract]
    public class MpLine : MpShape<Line>
    {
        #region Properties

        #region  StartPoint properties
        private MpPoint _startPoint;
        public MpPoint StartPoint
        {
            get { return _startPoint; }
            set
            {
                Set(ref _startPoint, value);
                Draw();
            }
        }

        private double _startPointX;
        public double StartPointX
        {
            get { return _startPointX; }
            set
            {
                if (value < 0)
                    return;
                var oldStartPoint = StartPoint;
                _startPoint.Remove();
                Set(ref _startPointX, value);
                StartPoint = new MpPoint(new Point(value, _startPointY), _canvasCollection)
                {
                    Description = oldStartPoint.Description,
                    DescriptionAlignment = oldStartPoint.DescriptionAlignment,
                    FillColor = oldStartPoint.FillColor
                };
            }
        }

        private double _startPointY;
        public double StartPointY
        {
            get { return _startPointY; }
            set
            {
                if (value < 0)
                    return;
                var oldStartPoint = StartPoint;
                _startPoint.Remove();
                Set(ref _startPointY, value);
                StartPoint = new MpPoint(new Point(_startPointX, value), _canvasCollection)
                {
                    Description = oldStartPoint.Description,
                    DescriptionAlignment = oldStartPoint.DescriptionAlignment,
                    FillColor = oldStartPoint.FillColor
                };
            }
        }
        #endregion

        #region EndPoint properties
        private MpPoint _endPoint;
        public MpPoint EndPoint
        {
            get { return _endPoint; }
            set
            {
                Set(ref _endPoint, value);
                Draw();
            }
        }

        private double _endPointX;
        public double EndPointX
        {
            get { return _endPointX; }
            set
            {
                if (value < 0)
                    return;
                _endPoint.Remove();
                Set(ref _endPointX, value);
                var oldEndPoint = EndPoint;
                EndPoint = new MpPoint(new Point(value, _endPointY), _canvasCollection)
                {
                    Description = oldEndPoint.Description,
                    DescriptionAlignment = oldEndPoint.DescriptionAlignment,
                    FillColor = oldEndPoint.FillColor
                };
            }
        }

        private double _endPointY;
        public double EndPointY
        {
            get { return _endPointY; }
            set
            {
                if (value < 0)
                    return;
                _endPoint.Remove();
                Set(ref _endPointY, value);
                var oldEndPoint = EndPoint;
                EndPoint = new MpPoint(new Point(_endPointX, value), _canvasCollection)
                {
                    Description = oldEndPoint.Description,
                    DescriptionAlignment = oldEndPoint.DescriptionAlignment,
                    FillColor = oldEndPoint.FillColor
                };
            }
        }
        #endregion

        private Color _borderColor;
        public Color BorderColor
        {
            get { return _borderColor; }
            set
            {
                _borderColor = value;
                Draw();
            }
        }

        private int _borderThickness;
        public int BorderThickness
        {
            get { return _borderThickness; }
            set
            {
                if (value < 0)
                    return;
                _borderThickness = value;
                Draw();
            }
        }

        #endregion

        private ObservableCollection<object> _canvasCollection;
        private bool _isAlreadyOnCanvas;

        /// <summary>
        /// Empty constructor
        /// </summary>
        public MpLine() { }

        /// <summary>
        /// Constructor that initializes object properties.
        /// </summary>
        /// <param name="id">Shape identification number.</param>
        /// <param name="canvasCollection"></param>
        public MpLine(int id, ObservableCollection<object> canvasCollection)
            : base(id, "Line" + id, new Line())
        {
            _canvasCollection = canvasCollection;
            _startPoint = new MpPoint(DefaultShapeProperties.GetInstance.StartPoint, _canvasCollection);
            _startPointX = DefaultShapeProperties.GetInstance.StartPoint.X;
            _startPointY = DefaultShapeProperties.GetInstance.StartPoint.Y;
            _endPoint = new MpPoint(DefaultShapeProperties.GetInstance.EndPoint, _canvasCollection)
            {
                Description = "B"
            };
            _endPointX = DefaultShapeProperties.GetInstance.EndPoint.X;
            _endPointY = DefaultShapeProperties.GetInstance.EndPoint.Y;
            _borderColor = DefaultShapeProperties.GetInstance.BorderColor;
            _borderThickness = DefaultShapeProperties.GetInstance.BorderThickness;

            _canvasCollection.Add(ShapeInstance);
            _isAlreadyOnCanvas = true;
        }

        public override void InitializeForDrawing(ObservableCollection<object> canvasCollection)
        {
            _canvasCollection = canvasCollection;
            StartPoint.InitializeForDrawing(canvasCollection);
            EndPoint.InitializeForDrawing(canvasCollection);
        }

        /// <summary>
        /// Draws current object.
        /// </summary>
        public override void Draw()
        {
            SetProperties();
            _startPoint.Draw();
            _endPoint.Draw();

            if (_isAlreadyOnCanvas) return;
            _canvasCollection.Add(ShapeInstance);
            _isAlreadyOnCanvas = true;
        }

        /// <summary>
        /// Sets current shape properties.
        /// </summary>
        private void SetProperties()
        {
            ShapeInstance.X1 = _startPoint.PositionX;
            ShapeInstance.Y1 = _startPoint.PositionY;
            ShapeInstance.X2 = _endPoint.PositionX;
            ShapeInstance.Y2 = _endPoint.PositionY;
            ShapeInstance.Stroke = new SolidColorBrush(BorderColor);
            ShapeInstance.StrokeThickness = BorderThickness;
        }

        /// <summary>
        /// Return current shape position (start point)
        /// </summary>
        /// <returns>Current shape position</returns>
        public override Point GetPosition()
        {
            return StartPoint.Position;
        }

        /// <summary>
        /// Move current object to given location.
        /// </summary>
        /// <param name="point"></param>
        /// <param name="canvasHeight"></param>
        /// <param name="canvasWidth"></param>
        public override void MoveTo(Point point, double canvasHeight, double canvasWidth)
        {
            var oldStartPoint = StartPoint;
            var oldEndPoint = EndPoint;
            var shift = _startPoint.DescriptionShift;

            if (point.X + shift > canvasWidth || point.Y + shift > canvasHeight || point.X - shift < 0 || point.Y - shift < 0 || point.X + shift + oldEndPoint.PositionX - oldStartPoint.PositionX > canvasWidth)
            {
                return;
            }

            StartPointX = point.X;
            StartPointY = point.Y;
            EndPointX = point.X + (oldEndPoint.PositionX - oldStartPoint.PositionX);
            EndPointY = point.Y + (oldEndPoint.PositionY - oldStartPoint.PositionY);
        }

        /// <summary>
        /// Converts current object into MetaPost language and saves it in a file.
        /// </summary>
        public override void ConvertIntoMetaPost(string path)
        {
            NumberFormatInfo nfi = new NumberFormatInfo {NumberDecimalSeparator = "."};
            double yMax = 842;  

            // Coordinates
            string strPositionX1 = Convert.ToString(StartPoint.Position.X);
            string strPositionY1 = Convert.ToString(yMax - StartPoint.Position.Y);
            string strPositionX2 = Convert.ToString(EndPoint.Position.X);
            string strPositionY2 = Convert.ToString(yMax - EndPoint.Position.Y);

            // Border color in RGB
            double borderColorR = Math.Round(BorderColor.R / 255.0, 3);
            double borderColorG = Math.Round(BorderColor.G / 255.0, 3);
            double borderColorB = Math.Round(BorderColor.B / 255.0, 3);

            // Start point fill color in RGB
            double startPointFillColorR = Math.Round(_startPoint.FillColor.R / 255.0, 3);
            double startPointFillColorG = Math.Round(_startPoint.FillColor.G / 255.0, 3);
            double startPointFillColorB = Math.Round(_startPoint.FillColor.B / 255.0, 3);

            // End point fill color in RGB
            double endPointFillColorR = Math.Round(_endPoint.FillColor.R / 255.0, 3);
            double endPointFillColorG = Math.Round(_endPoint.FillColor.G / 255.0, 3);
            double endPointFillColorB = Math.Round(_endPoint.FillColor.B / 255.0, 3);

            string[] mpCommands =
            {
                // Draw line
                $"% --- Line: Shape id: {Id} | Name: {Name} ---",
                $"draw ({strPositionX1},{strPositionY1})--({strPositionX2},{strPositionY2}) withcolor({borderColorR.ToString(nfi)},{borderColorG.ToString(nfi)},{borderColorB.ToString(nfi)}) withpen pensquare scaled {BorderThickness};",
                // Draw vertices
                $"draw ({strPositionX1},{strPositionY1}) withcolor({startPointFillColorR.ToString(nfi)},{startPointFillColorG.ToString(nfi)},{startPointFillColorB.ToString(nfi)}) withpen pencircle scaled {_startPoint.ShapeHeight};",
                $"label.{SpecifyPointDescPositionInMetaPost(_startPoint.DescriptionAlignment)}(btex ${_startPoint.Description}$ etex, ({strPositionX1},{strPositionY1}));",
                $"draw ({strPositionX2},{strPositionY2}) withcolor({endPointFillColorR.ToString(nfi)},{endPointFillColorG.ToString(nfi)},{endPointFillColorB.ToString(nfi)}) withpen pencircle scaled {_endPoint.ShapeHeight};",
                $"label.{SpecifyPointDescPositionInMetaPost(_endPoint.DescriptionAlignment)}(btex ${_endPoint.Description}$ etex, ({strPositionX2},{strPositionY2}));",

            };

            // Write commands to the file before "endfig;" line.
            List<string> lines = File.ReadLines(@path).ToList();
            string lineToFind = "endfig;";
            int index = lines.IndexOf(lineToFind);
            int i = 0;
            foreach (string mpCommand in mpCommands)
            {
                lines.Insert(index + i, mpCommand);
                i++;
            }
            File.WriteAllLines(@path, lines);
        }

        #region Data validation
        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "StartPointX":
                        if (StartPointX < 0)
                            return "The X value cannot be negative.";
                        break;
                    case "StartPointY":
                        if (StartPointY < 0)
                            return "The Y value cannot be negative.";
                        break;
                    case "EndPointX":
                        if (EndPointX < 0)
                            return "The X value cannot be negative.";
                        break;
                    case "EndPointY":
                        if (EndPointY < 0)
                            return "The Y value cannot be negative.";
                        break;
                    case "BorderThickness":
                        if (BorderThickness < 0)
                            return "The border thickness value cannot be negative.";
                        break;
                }

                return string.Empty;
            }
        }

        public string Error { get; }
        #endregion

        #region Serialization and deserialization
        /// <summary>
        /// Serialize MpLine object
        /// </summary>
        /// <param name="writer"></param>
        public override void Serialize(XmlWriter writer)
        {
            base.Serialize(writer);
            // Start point (Midpoint)
            writer.WriteStartElement(nameof(StartPoint));
                StartPoint.Serialize(writer);
            writer.WriteEndElement();
            // End point (Midpoint)
            writer.WriteStartElement(nameof(EndPoint));
                EndPoint.Serialize(writer);
            writer.WriteEndElement();
            // Border color
            writer.WriteStartElement(nameof(BorderColor));
                writer.WriteString(BorderColor.ToString());
            writer.WriteEndElement();
            // Border thickness
            writer.WriteStartElement(nameof(BorderThickness));
                writer.WriteString(BorderThickness.ToString());
            writer.WriteEndElement();
        }

        /// <summary>
        /// Deserialize MpLine object
        /// </summary>
        public override void Deserialize(XmlReader reader)
        {
            base.Deserialize(reader);

            _startPoint = new MpPoint(_canvasCollection);
            _endPoint = new MpPoint(_canvasCollection);

            // Start point
            _startPoint.Deserialize(reader);
            // Vertex B
            _endPoint.Deserialize(reader);
            // Border color
            _borderColor = (Color)ColorConverter.ConvertFromString(reader.ReadElementContentAsString());
            // Border thickness
            _borderThickness = reader.ReadElementContentAsInt();
        }
        #endregion
    }
}
