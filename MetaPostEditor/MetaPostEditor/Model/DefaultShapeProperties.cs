﻿using System.Windows;
using System.Windows.Media;

namespace MetaPostEditor.Model
{
    public class DefaultShapeProperties
    {
        private static DefaultShapeProperties _instance;

        public static DefaultShapeProperties GetInstance
        {
            get
            {
                if (_instance != null) return _instance;
                _instance = new DefaultShapeProperties();
                return _instance;
            }
        }

        // General properties
        public Point StartPoint { get; set; }
        public int ShapeHeight { get; set; }
        public int ShapeWidth { get; set; }
        public Color FillColor { get; set; }
        public Color BorderColor { get; set; }
        public int BorderThickness { get; set; }
        public enum DescriptionAllignmentType { Top, Right, Bottom, Left, TopRight, BottomRight, BottomLeft, TopLeft }

        // MpPolygon (triangle) properties
        public Point VertexA { get; set; }
        public Point VertexB { get; set; }
        public Point VertexC { get; set; }

        // MpLine properties
        public Point EndPoint { get; set; }

        // MpPoint properties
        public int PointHeight { get; set; }
        public string PointDescription { get; set; }
        public DescriptionAllignmentType DescriptionAlignment { get; set; }
        public double DescriptionShift { get; set; }
        public Color MpPointFillColor { get; set; }


        /// <summary>
        /// Constructor that initializes object properties.
        /// </summary>
        private DefaultShapeProperties()
        {
            StartPoint = new Point(100, 100);
            ShapeHeight = 100;
            ShapeWidth = 100;
            FillColor = Brushes.CadetBlue.Color;
            BorderColor = Brushes.DarkCyan.Color;
            BorderThickness = 2;

            // MpLine properties
            EndPoint = new Point(200, 100);

            // MpPoint properties
            PointHeight = 10;
            PointDescription = "A";
            DescriptionAlignment = DescriptionAllignmentType.Top;
            DescriptionShift = 12;
            MpPointFillColor = Brushes.DarkCyan.Color;
            
            // MpPolygon (triangle) properties
            VertexA = new Point(100, 100);
            VertexB = new Point(100, 190);
            VertexC = new Point(190, 190);
        }
    }
}
